#include "imageprocessing.h"
#include <stdio.h>
#include <stdlib.h>
#include "fixed_point.h"
#define PI 3.14159265

#define SHIFT_AMOUNT 8
#define SHIFT_MASK ((1 << SHIFT_AMOUNT) - 1) // 65535 (all LSB set, all MSB clear)




/*
rgb2gray converts RGB values to grayscale values by forming a weighted sum of the R, G, and B components:
0.2989 * R + 0.5870 * G + 0.1140 * B 
These are the same weights used to compute the Y component.
*/

void rgb2gray (GRAYImg *dst, RGBImg *src)
{
	int i,j;
	for (i = 0; i < src->rows; i++)
	{
		for (j = 0; j < src->cols; j++)
		{			
			dst->pixels[i][j] = (double) ((int) (0.21*src->pixels[i][j].R + 0.72*src->pixels[i][j].G + 0.07*src->pixels[i][j].B));					
		}
		
	}

}

void histogramGrayScale (GRAYImg *img, unsigned int *histogram)
{
	int i,j;
	/*Initializing the histogram vector*/
	for(i = 0; i < GRAYSCALE_LEVELS ; i++)
	{
		histogram[i] = 0;
	}

	for (i = 0; i < img->rows; ++i)
	{
		for(j = 0 ; j < img->cols ; ++j)
		{
			double index = img->pixels[i][j]*255;
			histogram[(int)index]++;
			//printf("%d\n", (int)index);
			//getchar();
		}
	}
}

double otsuLevel (GRAYImg *img)
{
	double threshold=0;
	int i,j;
	unsigned int histogram[GRAYSCALE_LEVELS];
	unsigned int totalPixels = img->rows*img->cols; // Total number of pixels in img
	histogramGrayScale(img,histogram); // Getting the histogram of img	
	
	double weightedSum = 0;
	double backgroundSum = 0;
	double maxVariance = 0;
	int wB=0; // Background Weight
	int wF=0; // Foreground Weight
	
	for (i = 0; i < GRAYSCALE_LEVELS ; i++)
	{
		weightedSum += (i/255.0)*((double) histogram[i]); // Total weighted sum

	}


	for(i = 0 ; i < GRAYSCALE_LEVELS ; i++)
	{
		wB +=histogram[i]; // Weight Background
		if(wB==0) continue;

		wF = totalPixels - wB;
		if(wF==0) break;

		backgroundSum+=  (i/255.0)*((double)histogram[i]);
		double meanBackground = backgroundSum/wB;
		double meanForeground = (weightedSum - backgroundSum)/wF;

		//Between Class Variance
		double betweenClassVariance = (double)wB * (double)wF * (meanBackground - meanForeground) * (meanBackground - meanForeground);
		
		//If there is a new maximum
		if(betweenClassVariance > maxVariance)
		{
			maxVariance = betweenClassVariance;
			threshold = ((double)i)/255.0;
		}
	}

	return threshold;
}

BINImg* otsuThreshold (GRAYImg* img)
{
	BINImg *binaryImage = createBINImg(img->rows,img->cols);
	double threshLevel = otsuLevel(img);
	unsigned int i,j;
	//printf("threshLevel %lf\n",threshLevel );
	for(i = 0 ; i < binaryImage->rows ; i++)
	{
		for(j = 0 ; j < binaryImage->cols ; j++)
		{
			if( img->pixels[i][j] >= threshLevel )
			{
				binaryImage->pixels[i][j] = 1;
			//	fprintf(file, "1 ");
			}
			else
			{
				binaryImage->pixels[i][j] = 0;
			//	fprintf(file, "0 ");
			}

		

		}
		//fprintf(file, "\n");
		
	}
	//GRAYorBINImgToMat(binaryImage);
	return binaryImage;
}



double getRValue (double s)
{
	//s = fabs(s);
	double R = 0;
	if (s+2 > 0)
	{
		R += (s+2)*(s+2)*(s+2);
	}
	if (s+1 > 0)
	{
		R -= 4*(s+1)*(s+1)*(s+1);
	}
	if (s > 0)
	{
		R += 6*s*s*s;
	}
	if (s-1 > 0)
	{
		R -= 4*(s-1)*(s-1)*(s-1);
	}
	return R*0.166666 /*R/6*/ ;

}

double getWvalue (double xf, int w) {
	switch(w) {
		case -1:
			return -0.5*xf*xf*xf + xf*xf - 0.5 *xf;
		case 0:
			return 1.5*xf*xf*xf - 2.5*xf*xf + 1;
		case 1: 
			return -1.5*xf*xf*xf + 2*xf*xf + 0.5*xf;
		case 2:
			return 0.5*xf*xf*xf - 0.5*xf*xf;
	}
}




/*Resize an image according to bicubic interpolation algorithm*/
GRAYImg *resize (GRAYImg *fnt, unsigned int rows, unsigned int cols)
{
	GRAYImg *resized = createGRAYImg(rows,cols);
	int i,j,m,n;
	double y_ratio = fnt->rows/(double)rows;
	double x_ratio = fnt->cols/(double)cols;

	//New value - old value
	double dx; // Xr - X
	double dy; // Yr - Y

	for(i = 0 ; i < rows ; i++)
	{
		for(j = 0 ; j < cols ; j++)
		{
			int x_old = (j*x_ratio);
			int y_old = (i*y_ratio); 
			

			resized->pixels[i][j] = 0;
			

		

			for(m = -1; m <= 2 ; m++)
			{
				for(n = -1 ; n <= 2 ; n++)
				{
					if( (y_old+m >= 0 && x_old+n >=0 ) && (y_old+m < fnt->rows-1 && x_old+n < fnt->cols-1))
					{
						dx = j*x_ratio - (x_old+n); // Xr - X
						dy = i*y_ratio - (y_old+m); 						

						//resized->pixels[i][j] +=  fnt->pixels[y_old+m][x_old+n]*getRValue(dx)*getRValue(dy);					
						resized->pixels[i][j] +=  fnt->pixels[y_old+m][x_old+n]*getWvalue(dx,n)*getWvalue(dy,m);
					}	
					else if (y_old+m == fnt->rows-1 || x_old+n == fnt->cols-1  ) 
					{
						resized->pixels[i][j] =   fnt->pixels[y_old][x_old];
					}					
				}
			}
			
			resized->pixels[i][j] = (double)  ((int) resized->pixels[i][j]) / 255;
		//	printf("%d",(int) resized->pixels[i][j]);
		//	getchar();

		

		}
		
	}
	
	return resized;
}


/*FIXED POINT*/

double fixedPointToDouble (int fp)
{
    return ((double)(fp >> SHIFT_AMOUNT)) + ((double)(fp & SHIFT_MASK) / (1 << SHIFT_AMOUNT));
}

int intToFixedPoint (int integer)
{
        return integer << SHIFT_AMOUNT;
}

int getRValue_fixedPoint (int s)
{
	//s = fabs(s);
	long int R = 0;
	int fp2 =intToFixedPoint(2);
	int fp1 = intToFixedPoint(1);

	//if(s<=0) return 0;

	if ( fixedPointToDouble(s+fp2) > 0)
	{
		R += (s+fp2)*(s+fp2)*(s+fp2);
	}
	if ( fixedPointToDouble(s+fp1) > 0)
	{
		R -= 4*(s+fp1)*(s+fp1)*(s+fp1);
	}
	if ( fixedPointToDouble(s) > 0)
	{
		R += 6*s*s*s;
	}
	if ( fixedPointToDouble(s-fp1) > 0)
	{
		R -= 4*(s-fp1)*(s-fp1)*(s-fp1);
	}
	//R = R << SHIFT_AMOUNT;
	
	return (R/6) >> SHIFT_AMOUNT*2 /*R/6*/ ;

}


//http://stackoverflow.com/questions/10067510/fixed-point-arithmetic-in-c-programming
GRAYImg *resizeFixedPoint (GRAYImg *fnt, unsigned int rows, unsigned int cols)
{
	GRAYImg *resized = createGRAYImg(rows,cols);
	int i,j,m,n;
	int source_rows = ((int)fnt->rows) << SHIFT_AMOUNT;
	int source_cols = ((int)fnt->cols) << SHIFT_AMOUNT;
	int y_ratio = source_rows/rows;
	int x_ratio = source_cols/cols;

	double y_ratio_d = fnt->rows/(double)rows;
	double x_ratio_d = fnt->cols/(double)cols;

	//printf("Fixed point: %d %d\n", y_old, x_old);
	//printf("Double: %d %d\n", y_old_d, x_old_d);
	//getchar();



	//New value - old value
	int dx; // Xr - X
	int dy; // Yr - Y

	for(i = 0 ; i < rows ; i++)
	{
		for(j = 0 ; j < cols ; j++)
		{
			int x_old = (j*x_ratio) >> SHIFT_AMOUNT;
			int y_old = (i*y_ratio) >> SHIFT_AMOUNT;

			int x_old_fp = (j*x_ratio);
			int y_old_fp = (i*y_ratio);

			resized->pixels[i][j] = 0;
								double resized_pf = 0;


			for(m = -1; m <= 2 ; m++)
			{
				for(n = -1 ; n <= 2 ; n++)
				{
					if( (y_old+m >= 0 && x_old+n >= 0 ) && (y_old+m < fnt->rows-1 && x_old+n < fnt->cols-1))
					{
						dx = j*x_ratio - intToFixedPoint(x_old) - intToFixedPoint(n); // Xr - X
						dy = i*y_ratio - intToFixedPoint(y_old) - intToFixedPoint(m); 	

						double dx_d = j*x_ratio_d - (x_old+n); // Xr - X
						double dy_d = i*y_ratio_d - (y_old+m); 

					//	printf("%g %g\n", fixedPointToDouble(dx), fixedPointToDouble( dy));

					

						
					//	printf("Fixed point: %g %g\n", fixedPointToDouble(getRValue_fixedPoint(dx)), fixedPointToDouble(getRValue_fixedPoint( dy)));
					//	printf("Double: %g %g\n", getRValue(dx_d), getRValue(dy_d));
					//	getchar();						
					
						//resized_pf += fnt->pixels[y_old+m][x_old+n]*getRValue(dx_d)*getRValue(dy_d);
			//			printf("%d %d\n", y_old+m, y_old+n);
			//			getchar();
						resized->pixels[i][j] +=  (((int)fnt->pixels[y_old+m][x_old+n]) * getRValue_fixedPoint(dx)   * getRValue_fixedPoint(dy)) >> 2*SHIFT_AMOUNT;					
			//	resized->pixels[i][j] =   fnt->pixels[y_old][x_old];			
					}	
					else if (y_old+m == fnt->rows-1 || x_old+n == fnt->cols-1) 
					{
						resized->pixels[i][j] =   fnt->pixels[y_old][x_old];
						n = m = 1000;
			//			resized_pf = fnt->pixels[y_old][x_old];
					}					
				}
			}
			//printf("done");
			//printf("%d : %d", (int) resized->pixels[i][j], (int) resized_pf); getchar();
			resized->pixels[i][j] = (double)  ((int) resized->pixels[i][j]) / 255;

		

		}
		
	}
	//getchar();
	return resized;
}



double *FPGA (GRAYImg *fnt, fpga_t* fpga)
{
	
	int i,j, indexFPGA_array; 
	int rows = fnt->rows;
	int cols = fnt->cols;
	int total_size = rows*cols;
	int words_amount = ceil(total_size/4.0);
	
	GRAYImg *resized = createGRAYImg(rows,cols);
	double *featuresVector = (double*) malloc(sizeof(double)*1296);

	int bit_mask = 0x0000001F;
	int shift_control = 0;


	unsigned int raw_pixels [words_amount+4]; // [rows*cols / 8]
	unsigned int received_pixels [128*128];

	indexFPGA_array = 2;
	raw_pixels[0] = rows;
	raw_pixels[1] = cols;
	raw_pixels[2] = 0;

	for(i = 0 ; i < rows ; i++) 
	{
		for( j = 0 ; j < cols ; j++) 
		{
			raw_pixels[indexFPGA_array] += (((unsigned int)fnt->pixels[i][j]) << shift_control);
			shift_control = (shift_control+8) & bit_mask; // this counter can not be higher than 24			
			if(shift_control == 0) // new word
			{
				indexFPGA_array++;
				raw_pixels[indexFPGA_array] = 0;
			}
		}
	}

	int sent = fpga_send(fpga, 0, raw_pixels, words_amount+4, 0, 1, 25000);
	int recvd = fpga_recv(fpga, 0, received_pixels, 1296, 25000);

	indexFPGA_array = 0;
	//shift_control = 0;	
	for(i = 0 ; i < 1296 ; i++)
	{
		featuresVector[i] = FixedPoint(received_pixels[indexFPGA_array]).to_double();
		indexFPGA_array++;
	}
	
	return featuresVector;
}





/*HOG FEATURES*/

Gradient *createGradient(unsigned int rows, unsigned int cols)
{
    unsigned int i;
    Gradient *gradient = (Gradient*) malloc(sizeof(Gradient));
    gradient->rows = rows;
    gradient->cols = cols;
    gradient->values = (vectorXY**) malloc(sizeof(vectorXY*)*rows);

    for(i = 0 ; i < rows ; i++)
    {
        gradient->values[i] = (vectorXY*) malloc(sizeof(vectorXY)*cols);
    }    
    return gradient;
}


Gradient * imageGradient (BINImg *img)
{
    unsigned int i,j;
    Gradient *gradient = createGradient (img->rows,img->cols);
   //GRAYImg *debug = createGRAYImg (img->rows,img->cols);
    unsigned int row_number = img->rows;
    unsigned int cols_number = img->cols;

    /*Values outside the boundaries are assumed to equal the nearest image border*/
    for(i = 0 ; i < row_number ; i++)
    {
        for(j = 0 ; j < cols_number ; j++)
        {
                /*Gradient in X direction*/           
                if(i==0)
                {
                  
                    gradient->values[i][j].dx = (double) img->pixels[0][j] - (double) img->pixels[i][j] ;
                }
                
                else if (i == row_number-1)
                {  
                        gradient->values[i][j].dx = (double) img->pixels[i][j] - (double) img->pixels[i-1][j];
                }    
                
                else
                {
                        gradient->values[i][j].dx = (double) img->pixels[i+1][j] - (double) img->pixels[i-1][j];
                }

                /*Gradient in Y direction*/

                 if(j==0)
                {
                                        

                    gradient->values[i][j].dy = img->pixels[i][0] - img->pixels[i][j] ;
                }
                
                else if (j == cols_number-1)
                {  
                        gradient->values[i][j].dy = img->pixels[i][j]- img->pixels[i][j-1];
                }    
                
                else
                {
                        gradient->values[i][j].dy = img->pixels[i][j+1] - img->pixels[i][j-1];
                }
      //   debug->pixels[i][j] = sqrt(gradient->values[i][j].dx *gradient->values[i][j].dx + gradient->values[i][j].dy*gradient->values[i][j].dy);
             //   gradient->values[i][j].dx /=255; 
               // gradient->values[i][j].dy /=255;

             //   printf("%f %f\n", gradient->values[i][j].dx,gradient->values[i][j].dy);
               // getchar();

           
        }
    }
   //GRAYorBINImgToMat(debug);   
    return gradient;

}

double getGradientMagnitude (double dx, double dy)
{
	return sqrt(dx*dx+dy*dy);
}

double getGradientAngle (double dx, double dy)
{
	//% Make the angles unsigned by adding pi (180 degrees) to all negative angles.	
	double angle;	
	angle = atan2(dy,dx);
	
	if(angle < 0 ) 
		angle = angle + PI;


	return angle;

}


/*histogramBins: Given a gradient, it returns the n-histogram bins. As we're interested only (for now) in the HOG features of a binary image, we'll use 
only 3 bins -> 0deg, 45deg, 90deg */
Histogram **histogramBins (Gradient *imgxy, unsigned int nBins,unsigned int cellSize)
{
	nBins = 9;
	
	/*For each cell, we'll have one histogram*/
	int i,j,l,m, indexHistogram=0;
	Histogram **histogram = (Histogram**) malloc(sizeof(Histogram*)*imgxy->rows/cellSize);
	for(i = 0 ; i < imgxy->rows/cellSize ; i++)
	{
		histogram[i] = (Histogram *) malloc(sizeof(Histogram)*imgxy->cols/cellSize);
		
		//histogram[i] = (double *) calloc((imgxy->cols/cellSize*nBins),sizeof(double));
	}

	for(i = 0 ; i < imgxy->rows/cellSize ; i++)
	{
		for(j = 0 ; j < imgxy->cols/cellSize ; j++)
		{
			histogram[i][j].values = (double*) calloc(nBins,sizeof(double));
		}
	}
		
	unsigned int histogramMap[9] = {10,30,50,70,90,110,130,150,170};
	
	int divisions = 180/*PI*//nBins;
	int firstAngle = 0;
	
	/*Calculating the histograms*/
	for(i = 0; i <= imgxy->rows - cellSize ; i = i + cellSize)
	{
		for(j = 0 ; j <= imgxy->cols -cellSize; j = j + cellSize)
		{
			/*Loop for each cell*/
						

			for(l = i ; l < i+cellSize && l < imgxy->rows ; l++)
			{												
							
					for(m = j ; m < j+cellSize && m < imgxy->cols  ; m++)
					{

					
						double angle = getGradientAngle(imgxy->values[l][m].dx,imgxy->values[l][m].dy)*180.0/PI;

						/*ANGLE INTERPOLATION */
						int leftBinIndex = round(angle/(double)divisions); // I just want the integer part
						leftBinIndex--; // to put in C format												
						int rightBinIndex = leftBinIndex + 1; //		
							if(leftBinIndex==-1)
							{
								leftBinIndex = (nBins-1);	
							}
							if(rightBinIndex==nBins)
							{
								rightBinIndex=0;	
							}
							
					double rightRatio = fabs(histogramMap[(leftBinIndex)] - angle)/(double)divisions;
					double leftRatio = 1 - rightRatio;

					if(leftBinIndex==nBins-1)
					{
						//double rightRatio = fabs(histogramMap[(leftBinIndex)] - angle)/(double)divisions;
						//double leftRatio = 1 - rightRatio;
						leftRatio = rightRatio = 0.5;
					}																																
				
						/*SPATIAL INTERPOLATION X,Y*/

						int x1 = floor(m/cellSize);
						int y1 = floor(l/cellSize);
						int z1 = floor(angle/divisions);
						if(z1==nBins) z1--;

						int x2 = x1+1;
						int y2 = y1+1;
						int z2 = z1+1;
						if(x2==imgxy->cols/cellSize) x2--; 
						if(y2==imgxy->cols/cellSize) y2--;
						if(z2==nBins) z2--;


					//	leftRatio = fabs( (angle-histogramMap[z1])/divisions - 1);
					//	rightRatio = fabs((angle-histogramMap[z1])/divisions);
					//	printf("passei 2\n");
					//	leftBinIndex = z1;
						//rightBinIndex = z2;

				//		printf("(x1,y1) = (%d,%d), (x2,y2) = (%d,%d), l: %d   m: %d\n", x1,y1,x2,y2,l,m);
				//		getchar();

						int row_hist = i/cellSize;
						int cols_hist = j/cellSize;

						int b1 = cellSize;
						

						double w_x1y1_left = fabs(((m-x1)/b1 - 1)*((l-y1)/b1 - 1)*leftRatio);
						double w_x1y1_right = fabs(((m-x1)/b1 - 1)*((l-y1)/b1 - 1)*rightRatio);




						double w_x1y2_left = fabs(((m-x1)/b1 - 1)*((l-y1)/b1)*leftRatio);
						double w_x1y2_right = fabs(((m-x1)/b1 - 1)*((l-y1)/b1)*rightRatio);

						double w_x2y1_left = fabs(((m-x1)/b1) * ((l-y1)/b1 - 1)*leftRatio);
						double w_x2y1_right = fabs(((m-x1)/b1)*((l-y1)/b1 - 1)*rightRatio);

						double w_x2y2_left = fabs(((m-x1)/b1)*((l-y1)/b1)*leftRatio);
						double w_x2y2_right = fabs(((m-x1)/b1)*((l-y1)/b1)*rightRatio);



						
						int blockSize = 36;
						double weight = getGradientMagnitude(imgxy->values[l][m].dx,imgxy->values[l][m].dy);//*pesos[l%blockSize][m%blockSize];

						if(0){
						printf("(x1,y1) = (%d,%d), (x2,y2) = (%d,%d), l: %d   m: %d\n", x1,y1,x2,y2,l,m);
						printf("angle: %f  weight: %f\n",angle,weight);
						printf("left %d right %d\n",z1,z2 );
						printf("left ratio: %lf right: %lf\n",leftRatio,rightRatio );
						printf("x1y1: %lf %lf\n", w_x1y1_left,w_x1y1_right);
						printf("x1y2: %lf %lf\n", w_x1y2_left,w_x1y2_right);
						printf("x2y1: %lf %lf\n", w_x2y1_left,w_x2y1_right);
						printf("x2y2: %lf %lf\n", w_x2y2_left,w_x2y2_right);
						//getchar();
					}

						histogram[y1][x1].values[leftBinIndex] += w_x1y1_left*weight;
						histogram[y1][x1].values[rightBinIndex] += w_x1y1_right*weight;
						
						histogram[y2][x1].values[leftBinIndex] += w_x1y2_left*weight;
						histogram[y2][x1].values[rightBinIndex] += w_x1y2_right*weight;
						
						histogram[y1][x2].values[leftBinIndex] += w_x2y1_left*weight;
						histogram[y1][x2].values[rightBinIndex] += w_x2y1_right*weight;

						histogram[y2][x2].values[leftBinIndex] += w_x2y2_left*weight;
						histogram[y2][x2].values[rightBinIndex] += w_x2y2_right*weight;
						
					//	printf("passei\n");
						//histogram[row_hist][cols_hist].values[leftBinIndex] += leftRatio*getGradientMagnitude(imgxy->values[l][m].dx,imgxy->values[l][m].dy);
						//histogram[row_hist][cols_hist].values[rightBinIndex] += rightRatio*getGradientMagnitude(imgxy->values[l][m].dx,imgxy->values[l][m].dy);
				
					}								
			}
//			getchar();
		
		}
	}
	return histogram;
}



double *normalizedHistogram (Histogram **histogram,unsigned int nBins,unsigned int blockSize, unsigned int cellSize, Gradient *imgxy)
{
	nBins = 9;
	unsigned int rows_histogram = imgxy->rows/cellSize;
	unsigned int cols_histogram = imgxy->cols/cellSize;
	unsigned int i,j,l,m,k,featuresCounter;
	double *featuresVector = (double*) malloc(sizeof(double)*(rows_histogram-blockSize/2)*(cols_histogram-blockSize/2)*(nBins*blockSize*blockSize));

	
	int numeroZeros = 0;
	featuresCounter = 0;
	for(i = 0 ; i < rows_histogram - blockSize/2 ; i++)
	{
		for(j = 0 ; j < cols_histogram - blockSize/2; j = j + (blockSize/2))
		{
			
			/*Calculating the vector module*/
			double acumulator = 0.0;
			for(l = i ; l < i+blockSize && l < rows_histogram; l++ )
			{
				for(m = j ; m < j+blockSize && m < cols_histogram; m++)
				{					
					for(k = 0 ; k < nBins ; k++)
					{
						acumulator += histogram[l][m].values[k]*histogram[l][m].values[k];

					}

				}
			}
			acumulator = sqrt(acumulator)+0.00001; // add a bias.
		
			/*Normalizing vectors*/
			double acumulator_2 = 0.0;
			int inicio = featuresCounter;
			for(l = i ; l < i+blockSize  && l < rows_histogram ; l++ )
			{
				for(m = j ; m < j+blockSize && m < cols_histogram; m++)
				{
					for(k = 0 ; k < nBins ; k++)
					{
						if(histogram[l][m].values[k]/acumulator + 0 != 0)
							numeroZeros++;

						featuresVector[featuresCounter++] = histogram[l][m].values[k]/acumulator;
						if(featuresVector[featuresCounter-1]>0.2)
							featuresVector[featuresCounter-1] = 0.2;
				//		acumulator_2 += featuresVector[featuresCounter-1]*featuresVector[featuresCounter-1];
	//					printf("%.10lf ",featuresVector[featuresCounter-1]);
						

					}	
					
				}
			}
		/*	acumulator_2 = sqrt(acumulator_2) + 0.001;
			
			for(k = inicio ; k < featuresCounter ; k++)
			{
				featuresVector[k]/=acumulator_2;								
			//	printf("%.10lf ", featuresVector[k]);
			}*/
		


		}
	}


	//printf("diferente de zero %d %d\n", numeroZeros,featuresCounter);
	//getchar();
	//printf("\n");
	return featuresVector;

}
