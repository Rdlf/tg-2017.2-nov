#ifndef IMAGEPROCESSING_INCLUDED
#define IMAGEPROCESSING_INCLUDED

#include "image.h"
#include "riffa.h"

#include <cmath>


typedef struct xy
{
	double dx;
	double dy;
}vectorXY;

typedef struct gradient
{
	unsigned int rows;
	unsigned int cols;
	vectorXY **values;
	
}Gradient;

/*Private*/
void histogramGrayScale (GRAYImg *img, unsigned int *histogram);// Auxiliar function of otsuThreshold
double otsuLevel (GRAYImg *img); // Auxiliar function of otsuThreshold
double getRValue (double s); //  // Auxiliar function of resize
Gradient *createGradient(unsigned int rows, unsigned int cols);

/*Public*/
void rgb2gray (GRAYImg *dst, RGBImg *src);
BINImg* otsuThreshold (GRAYImg* img);
GRAYImg *resize (GRAYImg *fnt, unsigned int rows, unsigned int cols);
GRAYImg *resizeFixedPoint (GRAYImg *fnt, unsigned int rows, unsigned int cols);
double *FPGA (GRAYImg *fnt, fpga_t *fpga);

Gradient * imageGradient (BINImg *img);
Histogram **histogramBins (Gradient *imgxy, unsigned int nBins,unsigned int cellSize);
double *normalizedHistogram (Histogram **histogram,unsigned int nBins,unsigned int blockSize, unsigned int cellSize,Gradient *imgxy);




#endif // IMAGEPROCESSING_INCLUDED
