#include "imageprocessing.h"
#include <stdio.h>
#include <stdlib.h>
#define PI 3.14159265



/*
rgb2gray converts RGB values to grayscale values by forming a weighted sum of the R, G, and B components:
0.2989 * R + 0.5870 * G + 0.1140 * B 
These are the same weights used to compute the Y component.
*/

void rgb2gray (GRAYImg *dst, RGBImg *src)
{
	int i,j;
	for (i = 0; i < src->rows; i++)
	{
		for (j = 0; j < src->cols; j++)
		{			
			dst->pixels[i][j] = (unsigned char) 0.2989*src->pixels[i][j].R + 0.5870*src->pixels[i][j].G + 0.1140*src->pixels[i][j].B;
		
		}
		
	}

}

void histogramGrayScale (GRAYImg *img, unsigned int *histogram)
{
	int i,j;
	/*Initializing the histogram vector*/
	for(i = 0; i < GRAYSCALE_LEVELS ; i++)
	{
		histogram[i] = 0;
	}

	for (i = 0; i < img->rows; ++i)
	{
		for(j = 0 ; j < img->cols ; ++j)
		{
			unsigned int pixelValue = img->pixels[i][j];
			histogram[pixelValue]++;
		}
	}
}

unsigned int otsuLevel (GRAYImg *img)
{
	unsigned int threshold=0;
	unsigned int i,j;
	unsigned int histogram[GRAYSCALE_LEVELS];
	unsigned int totalPixels = img->rows*img->cols; // Total number of pixels in img
	histogramGrayScale(img,histogram); // Getting the histogram of img	
	
	double weightedSum = 0;
	double backgroundSum = 0;
	double maxVariance = 0;
	int wB=0; // Background Weight
	int wF=0; // Foreground Weight
	
	for (i = 0; i < GRAYSCALE_LEVELS ; i++)
	{
		weightedSum += i*histogram[i]; // Total weighted sum
	}


	for(i = 0 ; i < GRAYSCALE_LEVELS ; i++)
	{
		wB +=histogram[i]; // Weight Background
		if(wB==0) continue;

		wF = totalPixels - wB;
		if(wF==0) break;

		backgroundSum+= (double) i*histogram[i];
		double meanBackground = backgroundSum/wB;
		double meanForeground = (weightedSum - backgroundSum)/wF;

		//Between Class Variance
		double betweenClassVariance = (double)wB * (double)wF * (meanBackground - meanForeground) * (meanBackground - meanForeground);
		
		//If there is a new maximum
		if(betweenClassVariance > maxVariance)
		{
			maxVariance = betweenClassVariance;
			threshold = i;
		}
	}

	return threshold;
}

BINImg* otsuThreshold (GRAYImg* img, FILE *file)
{
	BINImg *binaryImage = createBINImg(img->rows,img->cols);
	unsigned int threshLevel = otsuLevel(img);
	unsigned int i,j;

	for(i = 0 ; i < binaryImage->rows ; i++)
	{
		for(j = 0 ; j < binaryImage->cols ; j++)
		{
			if( img->pixels[i][j] >= threshLevel )
			{
				binaryImage->pixels[i][j] = 255;
			//	fprintf(file, "1 ");
			}
			else
			{
				binaryImage->pixels[i][j] = 0;
			//	fprintf(file, "0 ");
			}

		

		}
		//fprintf(file, "\n");
		
	}
	return binaryImage;
}



double getRValue (double s)
{
	//s = fabs(s);
	double R = 0;
	if (s+2 > 0)
	{
		R += (s+2)*(s+2)*(s+2);
	}
	if (s+1 > 0)
	{
		R -= 4*(s+1)*(s+1)*(s+1);
	}
	if (s > 0)
	{
		R += 6*s*s*s;
	}
	if (s-1 > 0)
	{
		R -= 4*(s-1)*(s-1)*(s-1);
	}
	return R/6;

}


/*Resize an image according to bicubic interpolation algorithm*/
GRAYImg *resize (GRAYImg *fnt, unsigned int rows, unsigned int cols)
{
	GRAYImg *resized = createGRAYImg(rows,cols);
	int i,j,m,n;
	double y_ratio = fnt->rows/(double)rows;
	double x_ratio = fnt->cols/(double)cols;

	//New value - old value
	double dx; // Xr - X
	double dy; // Yr - Y

	for(i = 0 ; i < rows ; i++)
	{
		for(j = 0 ; j < cols ; j++)
		{
			int x_old = (j*x_ratio);
			int y_old = (i*y_ratio); 
			

			resized->pixels[i][j] = 0;
		

			for(m = -1; m <= 2 ; m++)
			{
				for(n = -1 ; n <= 2 ; n++)
				{
					if( (y_old+m >= 0 && x_old+n >=0 ) && (y_old+m < fnt->rows-1 && x_old+n < fnt->cols-1))
					{
						dx = j*x_ratio - (x_old+n); // Xr - X
						dy = i*y_ratio - (y_old+m); 						

						resized->pixels[i][j] +=  fnt->pixels[y_old+m][x_old+n]*getRValue(dx)*getRValue(dy);					
					}	
					else if (y_old+m == fnt->rows-1 || x_old+n == fnt->cols-1  ) 
					{
						resized->pixels[i][j] =   fnt->pixels[y_old][x_old];
					}					
				}
			}

		

		}
		
	}
	
	return resized;
}



/*HOG FEATURES*/

Gradient *createGradient(unsigned int rows, unsigned int cols)
{
    unsigned int i;
    Gradient *gradient = (Gradient*) malloc(sizeof(Gradient));
    gradient->rows = rows;
    gradient->cols = cols;
    gradient->values = (vectorXY**) malloc(sizeof(vectorXY*)*rows);

    for(i = 0 ; i < rows ; i++)
    {
        gradient->values[i] = (vectorXY*) malloc(sizeof(vectorXY)*cols);
    }    
    return gradient;
}


Gradient * imageGradient (BINImg *img)
{
    unsigned int i,j;
    Gradient *gradient = createGradient (img->rows,img->cols);
   //GRAYImg *debug = createGRAYImg (img->rows,img->cols);
    unsigned int row_number = img->rows;
    unsigned int cols_number = img->cols;

    /*Values outside the boundaries are assumed to equal the nearest image border*/
    for(i = 0 ; i < row_number ; i++)
    {
        for(j = 0 ; j < cols_number ; j++)
        {
                /*Gradient in X direction*/           
                if(i==0)
                {
                  //  printf("Retornei!\n");
                    gradient->values[i][j].dx = (double) img->pixels[0][j] - (double) img->pixels[i][j] ;
                   // printf("Retornei!\n");
                }
                
                else if (i == row_number-1)
                {  
                        gradient->values[i][j].dx = (double) img->pixels[i][j] - (double) img->pixels[i-1][j];
                }    
                
                else
                {
                        gradient->values[i][j].dx = (double) img->pixels[i+1][j] - (double) img->pixels[i-1][j];
                }

                /*Gradient in Y direction*/

                 if(j==0)
                {
                                        

                    gradient->values[i][j].dy = img->pixels[i][0] - img->pixels[i][j] ;
                }
                
                else if (j == cols_number-1)
                {  
                        gradient->values[i][j].dy = img->pixels[i][j]- img->pixels[i][j-1];
                }    
                
                else
                {
                        gradient->values[i][j].dy = img->pixels[i][j+1] - img->pixels[i][j-1];
                }
      //   debug->pixels[i][j] = sqrt(gradient->values[i][j].dx *gradient->values[i][j].dx + gradient->values[i][j].dy*gradient->values[i][j].dy);
              //  gradient->values[i][j].dx /=255; 
            //    gradient->values[i][j].dy /=255;

             //   printf("%f %f\n", gradient->values[i][j].dx,gradient->values[i][j].dy);
               // getchar();

           
        }
    }
   //GRAYorBINImgToMat(debug);   
    return gradient;

}

double getGradientMagnitude (double dx, double dy)
{
	return sqrt(dx*dx+dy*dy);
}

double getGradientAngle (double dx, double dy)
{
	//% Make the angles unsigned by adding pi (180 degrees) to all negative angles.	
	double angle;	
	angle = atan2(dy,dx);
	
	if(angle < 0 ) 
		angle = angle + PI;


	return angle;

}


/*histogramBins: Given a gradient, it returns the n-histogram bins. As we're interested only (for now) in the HOG features of a binary image, we'll use 
only 3 bins -> 0deg, 45deg, 90deg */
Histogram **histogramBins (Gradient *imgxy, unsigned int nBins,unsigned int cellSize)
{
	nBins = 9;
	FILE *gaussian = fopen("gaussian.txt","r");
	double pesos[36][36];
	for(int i = 0 ; i < 36 ; i ++)
	{
		for(int j = 0 ; j < 36 ; j++)
		{
			fscanf(gaussian,"%lf ",&pesos[i][j]);
			//printf("%f ", pesos[i][j]);
		//	getchar();
		}
	}
	fclose(gaussian);
	/*For each cell, we'll have one histogram*/
	unsigned i,j,l,m, indexHistogram=0;
	Histogram **histogram = (Histogram**) malloc(sizeof(Histogram*)*imgxy->rows/cellSize);
	for(i = 0 ; i < imgxy->rows/cellSize ; i++)
	{
		histogram[i] = (Histogram *) malloc(sizeof(Histogram)*imgxy->cols/cellSize);
		
		//histogram[i] = (double *) calloc((imgxy->cols/cellSize*nBins),sizeof(double));
	}

	for(i = 0 ; i < imgxy->rows/cellSize ; i++)
	{
		for(j = 0 ; j < imgxy->cols/cellSize ; j++)
		{
			histogram[i][j].values = (double*) calloc(nBins,sizeof(double));
		}
	}


	

	//printf("%d %d %d",imgxy->rows/cellSize,imgxy->cols/cellSize,nBins);
//getchar();



	//double **histogram = (double **) calloc(imgxy->rows/cellSize,sizeof(double*));
	unsigned int histogramMap[9] = {10,30,50,70,90,110,130,150,170};
	
	int divisions = 180/*PI*//nBins;
	int firstAngle = 0;


	


	//*histogram = (unsigned int*)  malloc(sizeof(unsigned int)*(((int)imgxy->rows/cellSize)*((int)imgxy->cols/cellSize) *nBins));
		
	/*Calculating the histograms*/
	for(i = 0; i <= imgxy->rows - cellSize ; i = i + cellSize)
	{
		for(j = 0 ; j <= imgxy->cols -cellSize; j = j + cellSize)
		{
			/*Loop for each cell*/
						

			for(l = i ; l < i+cellSize && l < imgxy->rows ; l++)
			{												
							
					for(m = j ; m < j+cellSize && m < imgxy->cols  ; m++)
					{

					
						double angle = getGradientAngle(imgxy->values[l][m].dx,imgxy->values[l][m].dy)*180.0/PI;

						int leftBinIndex = round(angle/(double)divisions); // I just want the integer part
						leftBinIndex--; // to put in C format												
						int rightBinIndex = leftBinIndex + 1; //		
							if(leftBinIndex==-1)
							{
								leftBinIndex = (nBins-1);	
							}
							if(rightBinIndex==nBins)
							{
								rightBinIndex=0;	
							}
							
					double rightRatio = fabs(histogramMap[(leftBinIndex)] - angle)/(double)divisions;
					double leftRatio = 1 - rightRatio;

					if(leftBinIndex==nBins-1)
					{
						leftRatio = rightRatio = 0.5;
					}																																
				
						
						int row_hist = i/cellSize;
						int cols_hist = j/cellSize;

						//printf("%d %d %d %d %d\n", row_hist,cols_hist,m,l,m*l);
						//printf("%d %d %d %d\n", row_hist,cols_hist,leftBinIndex,rightBinIndex);
//printf("%f %f",leftRatio*getGradientMagnitude(imgxy->values[l][m].dx,imgxy->values[l][m].dy),rightRatio*getGradientMagnitude(imgxy->values[l][m].dx,imgxy->values[l][m].dy));
//printf("%d %d\n", row_hist,cols_hist);
//getchar();
						histogram[row_hist][cols_hist].values[leftBinIndex] += leftRatio*getGradientMagnitude(imgxy->values[l][m].dx,imgxy->values[l][m].dy);
						histogram[row_hist][cols_hist].values[rightBinIndex] += rightRatio*getGradientMagnitude(imgxy->values[l][m].dx,imgxy->values[l][m].dy);
					//	printf("im alive\n");
				
				//		histogram[i/cellSize][((nBins*j/cellSize))] the first element
				//		histogram[i/cellSize][((nBins*j/cellSize))+getGradientAngleIndex(imgxy->values[l][m].dx,imgxy->values[l][m].dy)] += getGradientMagnitude(imgxy->values[l][m].dx,imgxy->values[l][m].dy);
					}								
			}
//			getchar();
		
		}
	}
	return histogram;
}

double *normalizedHistogram (Histogram **histogram,unsigned int nBins,unsigned int blockSize, unsigned int cellSize, Gradient *imgxy)
{
	nBins = 9;
	unsigned int rows_histogram = imgxy->rows/cellSize;
	unsigned int cols_histogram = imgxy->cols/cellSize;
	unsigned int i,j,l,m,k,featuresCounter;
	double *featuresVector = (double*) malloc(sizeof(double)*(rows_histogram-blockSize/2)*(cols_histogram-blockSize/2)*(nBins*blockSize*blockSize));
//	printf("%d\n",(rows_histogram-blockSize/2)*(cols_histogram-blockSize/2)*(nBins*blockSize*blockSize) );
//	printf("%d %d\n",rows_histogram,cols_histogram);
//	getchar();
	int numeroZeros = 0;
	featuresCounter = 0;
	for(i = 0 ; i < rows_histogram - blockSize/2 ; i++)
	{
		for(j = 0 ; j < cols_histogram - blockSize/2; j = j + (blockSize/2))
		{
			
			/*Calculating the vector module*/
			double acumulator = 0.0;
			for(l = i ; l < i+blockSize && l < rows_histogram; l++ )
			{
				for(m = j ; m < j+blockSize && m < cols_histogram; m++)
				{					
					for(k = 0 ; k < nBins ; k++)
					{
//						printf("%d %d %d\n",l,m,k );
						acumulator += histogram[l][m].values[k]*histogram[l][m].values[k];
//						printf("%f\n",histogram[l][m].values[k]);
					}
//					printf("%f\n",acumulator);
//					getchar();
				}
			}
			acumulator = sqrt(acumulator)+0.001; // add a bias.
		
			/*Normalizing vectors*/
			for(l = i ; l < i+blockSize  && l < rows_histogram ; l++ )
			{
				for(m = j ; m < j+blockSize && m < cols_histogram; m++)
				{
					int inicio = featuresCounter;
					for(k = 0 ; k < nBins ; k++)
					{
						if(histogram[l][m].values[k]/acumulator + 0 != 0)
							numeroZeros++;

						featuresVector[featuresCounter++] = histogram[l][m].values[k]/acumulator;
						if(featuresVector[featuresCounter-1]>0.2)
							featuresVector[featuresCounter-1] = 0.2;
						printf("%f ",featuresVector[featuresCounter-1]);
						

					}	
					/*acumulator = 0;			
					for(k = inicio ; k < inicio + nBins ; k++)
					{
						acumulator+=featuresVector[k]*featuresVector[k];

					}
					acumulator = sqrt(acumulator) + 0.001;
					for(k = inicio ; k < inicio + nBins ; k++)
					{
						featuresVector[k]/=acumulator;
						printf("%f ",featuresVector[k]);

					}*/

				}
			}
		}
	}

/***********************RE-NORMALIZING*****************************************/

	/*double acumulator = 0;
		for(i = 0 ; i < 1296 ; i++)
	{		
		acumulator+= featuresVector[i]*featuresVector[i];
		//printf("%d %f",i,acumulator);
		//getchar();
	
	
			//printf("%f",acumulator );
			//getchar();
		
	
	
	

	}
	acumulator = sqrt(acumulator) + 0.001;
	for(j = 0 ; j < 1296 ; j++)
	{
		featuresVector[j] /=acumulator;				
		printf("%f ",featuresVector[j]);
		//	getchar();
	}

	//printf("diferente de zero %d %d\n", numeroZeros,featuresCounter);
	//getchar();*/
	printf("\n");
	return featuresVector;

}