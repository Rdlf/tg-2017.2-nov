/*
*
*
	IMAGE CONVERSIONS
*
*
*/
#include "image.h"

/*GRAYorBINImgToMat converts a GRAYImg or a BINImg into a cv::Mat*/
void GRAYorBINImgToMat (GRAYImg *src)
{	
	cv::Mat bw;
	bw = cv::Mat::zeros( src->rows, src->cols, CV_8UC1 );
  
	int i,j;
	for (i = 0; i < src->rows ; i++)
	{
		for (j = 0; j < src->cols; j++)
		{	
			bw.at<uchar>(i, j) = (uchar) src->pixels[i][j];
		}
	}
	
	cv::imshow( "thresh", bw );
	cv::waitKey(0);
}


/*RGBImgToMat converts from RGBImg to Mat (opencv)*/
void RGBImgToMat (cv::Mat dst, RGBImg *src)
{
	int i,j;
	for (i = 0; i < src->rows ; i++)
	{
		for (j = 0; j < src->cols; j++)
		{
			dst.at<cv::Vec3b>(i,j)[2] = src->pixels[i][j].R;
			dst.at<cv::Vec3b>(i,j)[1] = src->pixels[i][j].G;
			dst.at<cv::Vec3b>(i,j)[0] = src->pixels[i][j].B;
		}
	}

}

/*matToRGBImg converts from Mat (opencv) to RGBImg*/
void matToRGBImg (RGBImg *dst,cv::Mat src)
{
	int i,j;
	for (i = 0; i < src.rows ; i++)
	{
		for (j = 0; j < src.cols; j++)
		{
			dst->pixels[i][j].R = src.at<cv::Vec3b>(i,j)[2];
			dst->pixels[i][j].G = src.at<cv::Vec3b>(i,j)[1];
			dst->pixels[i][j].B = src.at<cv::Vec3b>(i,j)[0];
		}
	}

}






/*
*
*
		IMAGE CONSTRUCTORS
*
*/





/*createGRAYImg creates an empty GRAYImg matrix*/
GRAYImg* createGRAYImg (unsigned int rows, unsigned int cols)
{
	int i;
	GRAYImg *newImage;

	newImage = (GRAYImg*) malloc(sizeof(GRAYImg));
	newImage->rows = rows;
	newImage->cols = cols;
	newImage->pixels = (double **) malloc(sizeof(double*)*rows);
	for (i = 0; i < rows; i++)
	{
		newImage->pixels[i] = (double*) malloc(sizeof(double)*cols);
	}

	return newImage;

}


/*createBINImg creates an empty BINImg matrix*/
BINImg* createBINImg (unsigned int rows, unsigned int cols)
{
	int i;
	BINImg *newImage;

	newImage = (BINImg*) malloc(sizeof(BINImg));
	newImage->rows = rows;
	newImage->cols = cols;
	newImage->pixels = (double **) malloc(sizeof(double*)*rows);
	for (i = 0; i < rows; i++)
	{
		newImage->pixels[i] = (double*) malloc(sizeof(double)*cols);
	}

	return newImage;

}


/*createRGBImg creates an empty RGBImg matrix*/
RGBImg* createRGBImg (unsigned int rows, unsigned int cols)
{
	int i;
	RGBImg *newImage;

	newImage = (RGBImg*) malloc(sizeof(RGBImg));
	newImage->rows = rows;
	newImage->cols = cols;

	newImage->pixels = (RGB**) malloc(sizeof(RGB*)*rows);
	for (i = 0; i < rows; i++)
	{
		newImage->pixels[i] = (RGB*) malloc(sizeof(RGB)*cols);
	}

	return newImage;
}
