module histogramGrayScale (
		input clk,
		input rst,
		input start, 
		input [31:0] in,
		output bit [127:0] out, 
		output bit toOtsuThreshold
	);
	parameter GRAYSCALE_LEVELS = 255;
	parameter SHIFT_AMOUNT = 24;
	parameter SHIFT_MASK = ((1 << SHIFT_AMOUNT) - 1);

	//é preciso ter um a mais para enviar em pares
	bit [GRAYSCALE_LEVELS:0][15:0] histogram; //256*16 = 4096/4 = 1mb
	shortint i1, i2, i3, i4, i, j, count;
	int wB, wF, totalPixels;
	bit signed [127:0] backgroundSum, meanBackground, meanForeground, betweenClassVariance, maxVariance, weightedSum, temp;
	
 	integer data_file;
	integer scan_file;

	initial begin
		for (shortint k = 0; k < GRAYSCALE_LEVELS + 1; k++) begin
			histogram[k] = 0;
		end
		i = 0;
		j = 0;
		weightedSum = 0;
		wB = 0;
		totalPixels = 32'd16384;
		maxVariance = 0;
		backgroundSum = 0;
		toOtsuThreshold = 0;
		count = 0;
		//toOtsuLevel = 0;
		
 		// data_file = $fopen("histogramGrayScale.txt", "w");
		// if (data_file == 0) begin
			// $display("data_file handle was NULL");
			// $finish;
		// end
	end
	
	always @(posedge clk) begin
		if (rst) begin
			for (shortint k = 0; k < GRAYSCALE_LEVELS + 1; k++) begin
				histogram[k] = 0;
			end
			i = 0;
			j = 0;
			weightedSum = 0;
			wB = 0;
			totalPixels = 32'd16384;
			maxVariance = 0;
			backgroundSum = 0;
			toOtsuThreshold = 0;
			count = 0;
			//toOtsuLevel = 0;
		end
		
		//if (start) begin
			//4096 = (128x128)x8bits/32bits
			if (start & (count < 4096)) begin
				i1 = 16'(in >> 24);
				i2 = 16'((in >> 16) & 32'hFF);
				i3 = 16'((in >> 8) & 32'hFF);
				i4 = 16'(in & 32'hFF);
			
				histogram[i1]++;
				histogram[i2]++;
				histogram[i3]++;
				histogram[i4]++;

				count++;
			end 
			if (toOtsuThreshold == 0 & count >= 4096) begin
				//otsuLevel
				if (i < GRAYSCALE_LEVELS) begin
					i1 = histogram[i];
					i2 = histogram[i + 1];
						
					//out = {o1, o2};
						
					//i = i + 8'd2;
					
					//i1 = 16'(in >> 16);
					//i2 = 16'(in & 16'hFFFF);
					
					//histogram[i] = i1;
					//histogram[i + 1] = i2;

					//$fwrite(data_file, "i=%d histogram[i]=%d\n", i, histogram[i]);
					//$fwrite(data_file, "i=%d histogram[i]=%d\n", i+1, histogram[i+1]);
					
					//(i/255)*i1
					temp = (64'((i << SHIFT_AMOUNT) << SHIFT_AMOUNT) / (255 << SHIFT_AMOUNT)) * (i1 << SHIFT_AMOUNT);
					temp = temp + ((temp & 1 << (SHIFT_AMOUNT - 1)) << 1);
					temp >>>= SHIFT_AMOUNT;
					weightedSum = weightedSum + temp;
					//$fwrite(data_file, "i=%d i1=%d (i/255)*i1=%h weightedSum=%h\n", i, i1, temp, weightedSum);
					temp = (64'(((i + 1) << SHIFT_AMOUNT) << SHIFT_AMOUNT) / (255 << SHIFT_AMOUNT)) * (i2 << SHIFT_AMOUNT);
					temp = temp + ((temp & 1 << (SHIFT_AMOUNT - 1)) << 1);
					temp >>>= SHIFT_AMOUNT;
					weightedSum = weightedSum + temp;
					//$fwrite(data_file, "i=%d i2=%d (i/255)*i2=%h weightedSum=%h\n", i+1, i2, temp, weightedSum);
					
					i = i + 16'd2;
				end else begin
					if (j < GRAYSCALE_LEVELS) begin
						wB = wB + histogram[j];
						if (wB != 32'b0) begin	 
							wF = totalPixels - wB;
							if (wF != 32'b0) begin
								//(i/255)*i1
								temp = (64'((j << SHIFT_AMOUNT) << SHIFT_AMOUNT) / (255 << SHIFT_AMOUNT)) * (histogram[j] << SHIFT_AMOUNT);
								temp = temp + ((temp & 1 << (SHIFT_AMOUNT - 1)) << 1);
								temp >>>= SHIFT_AMOUNT;
								backgroundSum = backgroundSum + temp;
								//meanBackground = backgroundSum / wB;
								meanBackground = 64'(backgroundSum << SHIFT_AMOUNT) / (wB << SHIFT_AMOUNT);
								//meanForeground = (weightedSum - backgroundSum) / wF;
								meanForeground = 64'((weightedSum - backgroundSum) << SHIFT_AMOUNT) / (wF << SHIFT_AMOUNT);
								//betweenClassVariance = (double)wB * (double)wF * (meanBackground - meanForeground) * (meanBackground - meanForeground);
								temp = (wB << SHIFT_AMOUNT) * (wF << SHIFT_AMOUNT);
								temp = temp + ((temp & 1 << (SHIFT_AMOUNT - 1)) << 1);
								temp >>>= SHIFT_AMOUNT;
								temp = temp * (meanBackground - meanForeground);
								temp = temp + ((temp & 1 << (SHIFT_AMOUNT - 1)) << 1);
								temp >>>= SHIFT_AMOUNT;
								temp = temp * (meanBackground - meanForeground);
								temp = temp + ((temp & 1 << (SHIFT_AMOUNT - 1)) << 1);
								temp >>>= SHIFT_AMOUNT;
								betweenClassVariance = temp;
								
								if (betweenClassVariance > maxVariance) begin
									maxVariance = betweenClassVariance;
									out = 64'((j << SHIFT_AMOUNT) << SHIFT_AMOUNT) / (255 << SHIFT_AMOUNT);
								end
								
								// $fwrite(data_file,"weightedSum:%h\n", weightedSum);
								// $fwrite(data_file, "j=%d\n wB=%d\n wF=%d\n backgroundSum=%h\nmeanBackground=%h meanForeground=%h\maxVariance=%h\n out=%h\n", j, wB, wF, backgroundSum, meanBackground, meanForeground, maxVariance, out);

								//if (j == GRAYSCALE_LEVELS) begin
								//	$fclose(data_file);			
								//end
							end
						end
						
						j++;
					end else begin
						toOtsuThreshold = 1;
					end
				
					// if (toOtsuLevel == 0) begin
						// toOtsuLevel = 1;
					// end
				end
			end
		//end
	end
endmodule