import re

with open('featuresVector_imagem_1_double.txt','wb') as cv:
  with open('featuresVector_imagem_1.txt') as fp:
    for line in fp:
      value =  re.search(r'[0-9a-f]{32}',line)
      if value:
        value = value.group()
        #print value
        value_int = int(value, 16)
        value_hex = hex(value_int)
        SHIFT_AMOUNT = 27
        SHIFT_MASK = ((1 << SHIFT_AMOUNT) - 1)
        #print value
        #print value_int
        #print value_hex
        #print hex(value_int >> SHIFT_AMOUNT)
        cv.write(re.sub(r'[0-9a-f]{32}',str((value_int >> SHIFT_AMOUNT) + (value_int & SHIFT_MASK) / float(1 << SHIFT_AMOUNT)), line))
        #break
      else:
        cv.write('end\n')

file_c = open('featuresVector_imagem_1_c.txt', 'r')
file_modelsim = open('featuresVector_imagem_1_double.txt', 'r')
result = open('result.txt','wb')

erros = 0
for line in file_modelsim:
  line2 = file_c.readline()
  if line.find('end') != 0:
    #print line, line2
    erros += abs(float(line) - float(line2))
    result.write("%.10f"%(abs(float(line) - float(line2)))+'\n')
    #result.write(line[:-1]+' '+line2)
    #result.write('\n')
  else:
    result.write('end\n')

result.write("erros:")
result.write("%.10f"%erros+'\n')
file_c.close()
file_modelsim.close()
result.close()
