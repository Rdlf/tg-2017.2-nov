module histogramBins (
		input clk,
		input rst,
		input start,
		input int q_a,
		input int q_b,
		output bit [11:0] address_a,
		output bit [11:0] address_b,
		output bit [127:0] featuresVector_out,
		output bit end_signal
	);
	parameter SHIFT_AMOUNT = 27;
	parameter SHIFT_MASK = ((1 << SHIFT_AMOUNT) - 1);
	
	bit signed [127:0] temp, aux, angle, rightRatio, leftRatio, w_x1y1_left, w_x1y1_right, w_x1y2_left, w_x1y2_right, w_x2y1_left, w_x2y1_right, w_x2y2_left, w_x2y2_right, weight, acumulator, sqrt_acumulator;
	bit [127:0] temp2, aux2;
	bit signed [127:0] atan2[3][3], getGradientMagnitude[3][3];
	bit signed [63:0] histogram[7][7][9]; //histogram = 4mb
	shortint leftBinIndex, rightBinIndex, x1, y1, z1, x2, y2, z2, b1, mod_a, mod_b;
	bit signed [15:0] histogramMap[9];
	byte count, io, jo, lo, mo, dx, dy, ko, step, e;
	bit toNormalizedHistogram;
	
	bit [7:0] out;
	bit [127:0] in;
	
	highbit highbit(.in(in), .out(out));
  
	integer data_file;
	integer scan_file;

	initial begin
		atan2 = '{'{128'h6487ed4, 128'h1921fb54, 128'h12d97c7e}, '{128'hc90fda9, 128'h0, 128'hc90fdaa}, '{128'h12d97c7e, 128'h0, 128'h6487ed4}};
		getGradientMagnitude = '{'{128'hb504f33, 128'h8000000, 128'hb504f33}, '{128'h8000000, 128'h0, 128'h8000000}, '{128'hb504f33, 128'h8000000, 128'hb504f33}};
		histogram = '{'{'{0,0,0,0,0,0,0,0,0},'{0,0,0,0,0,0,0,0,0},'{0,0,0,0,0,0,0,0,0},'{0,0,0,0,0,0,0,0,0},'{0,0,0,0,0,0,0,0,0},'{0,0,0,0,0,0,0,0,0},'{0,0,0,0,0,0,0,0,0}},'{'{0,0,0,0,0,0,0,0,0},'{0,0,0,0,0,0,0,0,0},'{0,0,0,0,0,0,0,0,0},'{0,0,0,0,0,0,0,0,0},'{0,0,0,0,0,0,0,0,0},'{0,0,0,0,0,0,0,0,0},'{0,0,0,0,0,0,0,0,0}},'{'{0,0,0,0,0,0,0,0,0},'{0,0,0,0,0,0,0,0,0},'{0,0,0,0,0,0,0,0,0},'{0,0,0,0,0,0,0,0,0},'{0,0,0,0,0,0,0,0,0},'{0,0,0,0,0,0,0,0,0},'{0,0,0,0,0,0,0,0,0}},'{'{0,0,0,0,0,0,0,0,0},'{0,0,0,0,0,0,0,0,0},'{0,0,0,0,0,0,0,0,0},'{0,0,0,0,0,0,0,0,0},'{0,0,0,0,0,0,0,0,0},'{0,0,0,0,0,0,0,0,0},'{0,0,0,0,0,0,0,0,0}},'{'{0,0,0,0,0,0,0,0,0},'{0,0,0,0,0,0,0,0,0},'{0,0,0,0,0,0,0,0,0},'{0,0,0,0,0,0,0,0,0},'{0,0,0,0,0,0,0,0,0},'{0,0,0,0,0,0,0,0,0},'{0,0,0,0,0,0,0,0,0}},'{'{0,0,0,0,0,0,0,0,0},'{0,0,0,0,0,0,0,0,0},'{0,0,0,0,0,0,0,0,0},'{0,0,0,0,0,0,0,0,0},'{0,0,0,0,0,0,0,0,0},'{0,0,0,0,0,0,0,0,0},'{0,0,0,0,0,0,0,0,0}},'{'{0,0,0,0,0,0,0,0,0},'{0,0,0,0,0,0,0,0,0},'{0,0,0,0,0,0,0,0,0},'{0,0,0,0,0,0,0,0,0},'{0,0,0,0,0,0,0,0,0},'{0,0,0,0,0,0,0,0,0},'{0,0,0,0,0,0,0,0,0}}};
		histogramMap = '{16'd10, 16'd30, 16'd50, 16'd70, 16'd90, 16'd110, 16'd130, 16'd150, 16'd170};
		io = 0;
		jo = 0;
				 
		lo = 0;
		mo = 0;
		count = 0;
		address_a = 0;
		address_b = 0;
		toNormalizedHistogram = 0;
		ko = 0;
		step = 0;
		//featuresCounter = 0;
		acumulator = 0;
		end_signal = 0;
		
 
		data_file = $fopen("featuresVector.txt", "w");
		if (data_file == 0) begin
			$display("data_file handle was NULL");
			$finish;
		end
	end
	
	always @(posedge clk) begin
		if (rst) begin
			histogram = '{'{'{0,0,0,0,0,0,0,0,0},'{0,0,0,0,0,0,0,0,0},'{0,0,0,0,0,0,0,0,0},'{0,0,0,0,0,0,0,0,0},'{0,0,0,0,0,0,0,0,0},'{0,0,0,0,0,0,0,0,0},'{0,0,0,0,0,0,0,0,0}},'{'{0,0,0,0,0,0,0,0,0},'{0,0,0,0,0,0,0,0,0},'{0,0,0,0,0,0,0,0,0},'{0,0,0,0,0,0,0,0,0},'{0,0,0,0,0,0,0,0,0},'{0,0,0,0,0,0,0,0,0},'{0,0,0,0,0,0,0,0,0}},'{'{0,0,0,0,0,0,0,0,0},'{0,0,0,0,0,0,0,0,0},'{0,0,0,0,0,0,0,0,0},'{0,0,0,0,0,0,0,0,0},'{0,0,0,0,0,0,0,0,0},'{0,0,0,0,0,0,0,0,0},'{0,0,0,0,0,0,0,0,0}},'{'{0,0,0,0,0,0,0,0,0},'{0,0,0,0,0,0,0,0,0},'{0,0,0,0,0,0,0,0,0},'{0,0,0,0,0,0,0,0,0},'{0,0,0,0,0,0,0,0,0},'{0,0,0,0,0,0,0,0,0},'{0,0,0,0,0,0,0,0,0}},'{'{0,0,0,0,0,0,0,0,0},'{0,0,0,0,0,0,0,0,0},'{0,0,0,0,0,0,0,0,0},'{0,0,0,0,0,0,0,0,0},'{0,0,0,0,0,0,0,0,0},'{0,0,0,0,0,0,0,0,0},'{0,0,0,0,0,0,0,0,0}},'{'{0,0,0,0,0,0,0,0,0},'{0,0,0,0,0,0,0,0,0},'{0,0,0,0,0,0,0,0,0},'{0,0,0,0,0,0,0,0,0},'{0,0,0,0,0,0,0,0,0},'{0,0,0,0,0,0,0,0,0},'{0,0,0,0,0,0,0,0,0}},'{'{0,0,0,0,0,0,0,0,0},'{0,0,0,0,0,0,0,0,0},'{0,0,0,0,0,0,0,0,0},'{0,0,0,0,0,0,0,0,0},'{0,0,0,0,0,0,0,0,0},'{0,0,0,0,0,0,0,0,0},'{0,0,0,0,0,0,0,0,0}}};
			io = 0;
			jo = 0;
					
			lo = 0;
			mo = 0;
			count = 0;
			address_a = 0;
			address_b = 0;
			toNormalizedHistogram = 0;
			ko = 0;
			step = 0;
			//featuresCounter = 0;
			acumulator = 0;
			end_signal = 0;
		end
		
		if (end_signal == 1) begin
			end_signal = 0;
		end
		
		if (start & toNormalizedHistogram == 0) begin
			if (count == 0) begin
				if (lo == 0) begin
					address_a = 12'((lo * 128 + mo) / 4);
					address_b = 12'((lo * 128 + mo) / 4);
					mod_a = 16'((lo * 128 + mo) % 4);
					mod_b = 16'((lo * 128 + mo) % 4);
				end else if (lo == 128 - 1) begin
					address_a = 12'((lo * 128 + mo) / 4);
					address_b = 12'(((lo - 1) * 128 + mo) / 4);
					mod_a = 16'((lo * 128 + mo) % 4);
					mod_b = 16'(((lo - 1) * 128 + mo) % 4);
				end else begin
					address_a = 12'(((lo + 1) * 128 + mo) / 4);
					address_b = 12'(((lo - 1) * 128 + mo) / 4);
					mod_a = 16'(((lo + 1) * 128 + mo) % 4);
					mod_b = 16'(((lo - 1) * 128 + mo) % 4);
				end
			$fwrite(data_file,"lo:%d mo:%d\n",lo,mo);
				//$fwrite(data_file, "a=%d b=%d mod_a=%d mod_b=%d\n", address_a, address_b, mod_a, mod_b);
				//$fwrite(data_file, "count=%d\n", debug);
			end
			
			if (count == 4) begin
			$fwrite(data_file, "address_a=%d address_b=%d q_a=%d q_b=%d\n", 14'(4*address_a+mod_a), 14'(4*address_b+mod_b), 8'((q_a >> (8 * (3 - mod_a))) & 8'hFF), 8'((q_b >> (8 * (3 - mod_b))) & 8'hFF));
				if (lo == 0) begin
					dx = 0;
				end else begin
					dx = 8'(((q_a >> $unsigned(8 * (3 - mod_a))) & 8'hFF) - ((q_b >> $unsigned(8 * (3 - mod_b))) & 8'hFF));
				end
				//$fwrite(data_file, "count=%d\n", debug);
				//$fwrite(data_file, "i=%d j=%d q_a=%h q_b=%h\n", lo, mo, ((q_a >> (8 * (3 - mod_a))) & 8'hFF), ((q_b >> (8 * (3 - mod_b))) & 8'hFF));
				//$fwrite(data_file, "x: a=%d b=%d dx=%d dy=%d\n", address_a, address_b, dx, dy);
				
				if (mo == 0) begin
					address_a = 12'((lo * 128 + mo) / 4);
					address_b = 12'((lo * 128 + mo) / 4);
					mod_a = 16'((lo * 128 + mo) % 4);
					mod_b = 16'((lo * 128 + mo) % 4);
				end else if (mo == 128 - 1) begin
					address_a = 12'((lo * 128 + mo) / 4);
					address_b = 12'((lo * 128 + (mo - 1)) / 4);
					mod_a = 16'((lo * 128 + mo) % 4);
					mod_b = 16'((lo * 128 + (mo - 1)) % 4);
				end else begin
					address_a = 12'((lo * 128 + (mo + 1)) / 4);
					address_b = 12'((lo * 128 + (mo - 1)) / 4);
					mod_a = 16'((lo * 128 + (mo + 1)) % 4);
					mod_b = 16'((lo * 128 + (mo - 1)) % 4);
				end
			$fwrite(data_file,"lo:%d mo:%d\n",lo,mo);
			end
			
			if (count == 7) begin
			$fwrite(data_file, "address_a=%d address_b=%d q_a=%d q_b=%d\n", 14'(4*address_a+mod_a), 14'(4*address_b+mod_b), 8'((q_a >> (8 * (3 - mod_a))) & 8'hFF), 8'((q_b >> (8 * (3 - mod_b))) & 8'hFF));
				if (mo == 0) begin
					dy = 0;
				end else begin
					dy = 8'(((q_a >> $unsigned(8 * (3 - mod_a))) & 8'hFF) - ((q_b >> $unsigned(8 * (3 - mod_b))) & 8'hFF));
				end
			$fwrite(data_file,"dx:%d dy:%d\n",dx,dy);
				//$fwrite(data_file, "count=%d\n", debug);
				//$fwrite(data_file, "i=%d j=%d q_a=%h q_b=%h\n", lo, mo, ((q_a >> (8 * (3 - mod_a))) & 8'hFF), ((q_b >> (8 * (3 - mod_b))) & 8'hFF));
				//$fwrite(data_file, "i=%d j=%d dx=%d dy=%d\n", lo, mo, dx, dy);
				
				if (io <= 128 - 18) begin
					if (jo <= 128 - 18) begin
						if (lo < io + 18 && lo < 128) begin
							if (mo < jo + 18 && mo < 128) begin
								// angle*180.0 / PI
								temp = atan2[dx + 1][dy + 1] * 127'h1ca5dc1af;
								temp = temp + ((temp & 1 << (SHIFT_AMOUNT - 1)) << 1);
								temp >>>= SHIFT_AMOUNT;
								angle = temp;
								//angle = atan2[dx + 1][dy + 1];
								//if (angle != 0) begin
									//$fwrite(data_file, "angle=%h dx=%d dy=%d\n", atan2[dx + 1][dy + 1], dx, dy);
									//$fwrite(data_file, "i=%d j=%d l=%d m=%d angle=%d\n", io, jo, lo, mo, angle);
									//$fwrite(data_file, "y: a=%d b=%d pixel_a=%d pixel_b=%d\n", address_a, address_b, q_a, q_b);
									//$fwrite(data_file, "angle=%h angle*180=%h\n",atan2[dx + 1][dy + 1], temp);
								//end
								
								temp = (angle << SHIFT_AMOUNT) / (20 << SHIFT_AMOUNT);
								temp = temp + (1 << (SHIFT_AMOUNT - 1));
								leftBinIndex = 16'(temp >>> SHIFT_AMOUNT);
								leftBinIndex--;
								rightBinIndex = 16'(leftBinIndex + 1);
								if (leftBinIndex == -1) begin
									leftBinIndex = 16'(9 - 1);
								end
								if (rightBinIndex == 9) begin
									rightBinIndex = 0;
								end
		
			$fwrite(data_file,"leftBinIndex:%d rightBinIndex:%d\n",leftBinIndex,rightBinIndex);
								//$fwrite(data_file, "i=%d j=%d l=%d m=%d leftBinIndex=%d rightBinIndex=%d get=%h angle=%h\n", io, jo, lo, mo, leftBinIndex, rightBinIndex, atan2[dx + 1][dy + 1], angle);
								//$fwrite(data_file, "i=%d j=%d l=%d m=%d leftBinIndex=%d rightBinIndex=%d\n", io, jo, lo, mo, leftBinIndex, rightBinIndex);

								temp = (histogramMap[leftBinIndex] << SHIFT_AMOUNT) - angle;
								//$fwrite(data_file, "temp1=%h\n", temp);
								if (temp < 0) begin
									temp *= -1;
								end
								//$fwrite(data_file, "temp2=%h\n", temp);
								temp = (temp << SHIFT_AMOUNT) / (20 << SHIFT_AMOUNT);
								rightRatio = temp;
								leftRatio = (1 << SHIFT_AMOUNT) - rightRatio;

								if (leftBinIndex == (9 - 1)) begin
									leftRatio = 128'h4000000;
									rightRatio = 128'h4000000;
								end
			$fwrite(data_file,"rightRatio:%d leftRatio:%d\n",rightRatio,leftRatio);
								//$fwrite(data_file, "i=%d j=%d l=%d m=%d leftRatio=%h rightRatio=%h leftBinIndex=%d\n", io, jo, lo, mo, leftRatio, rightRatio, leftBinIndex);

								x1 = 16'(mo / 18);
								y1 = 16'(lo / 18);
								z1 = 16'(((angle << SHIFT_AMOUNT) / (20 << SHIFT_AMOUNT)) >>> SHIFT_AMOUNT);
								if (z1 == 9) z1--;
			$fwrite(data_file,"x1:%d y1:%d\n",x1,y1);

								x2 = 16'(x1 + 1);
								y2 = 16'(y1 + 1);
								z2 = 16'(z1 + 1);
								if (x2 == 128 / 18) x2--;
								if (y2 == 128 / 18) y2--;
								if (z2 == 9) z2--;
								//$fwrite(data_file, "i=%d j=%d l=%d m=%d x1=%d y1=%d z1=%d x2=%d y2=%d z2=%d\n", io, jo, lo, mo, x1, y1, z1, x2, y2, z2);
								
								//row_hist = io / 18;
								//cols_hist = jo / 18;
								b1 = 18;
								
								temp = ((mo - x1) / b1) - 1;
								aux = ((lo - y1) / b1) - 1;
								aux = temp * aux;
								aux = aux << SHIFT_AMOUNT;
								
								temp = aux * leftRatio;
								temp = temp + ((temp & 1 << (SHIFT_AMOUNT - 1)) << 1);
								temp >>>= SHIFT_AMOUNT;
								if (temp < 0) begin
									temp *= -1;
								end
								w_x1y1_left = temp;
								
								temp = aux * rightRatio;
								temp = temp + ((temp & 1 << (SHIFT_AMOUNT - 1)) << 1);
								temp >>>= SHIFT_AMOUNT;
								if (temp < 0) begin
									temp *= -1;
								end
								w_x1y1_right = temp;
								
								temp = ((mo - x1) / b1) - 1;
								aux = ((lo - y1) / b1);
								aux = temp * aux;
								aux = aux << SHIFT_AMOUNT;
								
								temp = aux * leftRatio;
								temp = temp + ((temp & 1 << (SHIFT_AMOUNT - 1)) << 1);
								temp >>>= SHIFT_AMOUNT;
								if (temp < 0) begin
									temp *= -1;
								end
								w_x1y2_left = temp;
								
								temp = aux * rightRatio;
								temp = temp + ((temp & 1 << (SHIFT_AMOUNT - 1)) << 1);
								temp >>>= SHIFT_AMOUNT;
								if (temp < 0) begin
									temp *= -1;
								end							
								w_x1y2_right = temp;
								
								temp = ((mo - x1) / b1);
								aux = ((lo - y1) / b1) - 1;
								aux = temp * aux;
								aux = aux << SHIFT_AMOUNT;
								
								temp = aux * leftRatio;
								temp = temp + ((temp & 1 << (SHIFT_AMOUNT - 1)) << 1);
								temp >>>= SHIFT_AMOUNT;
								if (temp < 0) begin
									temp *= -1;
								end
								w_x2y1_left = temp;
								
								temp = aux * rightRatio;
								temp = temp + ((temp & 1 << (SHIFT_AMOUNT - 1)) << 1);
								temp >>>= SHIFT_AMOUNT;
								if (temp < 0) begin
									temp *= -1;
								end
								w_x2y1_right = temp;
								
								temp = ((mo - x1) / b1);
								aux = ((lo - y1) / b1);
								aux = temp * aux;
								aux = aux << SHIFT_AMOUNT;
								
								temp = aux * leftRatio;
								temp = temp + ((temp & 1 << (SHIFT_AMOUNT - 1)) << 1);
								temp >>>= SHIFT_AMOUNT;
								if (temp < 0) begin
									temp *= -1;
								end
								w_x2y2_left = temp;
								
								temp = aux * rightRatio;
								temp = temp + ((temp & 1 << (SHIFT_AMOUNT - 1)) << 1);
								temp >>>= SHIFT_AMOUNT;
								if (temp < 0) begin
									temp *= -1;
								end
								w_x2y2_right = temp;
			$fwrite(data_file,"w_x1y1_left:%d w_x1y1_right:%d\n",w_x1y1_left,w_x1y1_right);
			$fwrite(data_file,"w_x1y2_left:%d w_x1y2_right:%d\n",w_x1y2_left,w_x1y2_right);
			$fwrite(data_file,"w_x2y1_left:%d w_x2y1_right:%d\n",w_x2y1_left,w_x2y1_right);
			$fwrite(data_file,"w_x2y2_left:%d w_x2y2_right:%d\n",w_x2y2_left,w_x2y2_right);
								
								//$fwrite(data_file, "i=%d j=%d l=%d m=%d w_x1y1_left=%h w_x1y1_right=%h w_x1y2_left=%h w_x1y2_right=%h w_x2y1_left=%h w_x2y1_right=%h w_x2y2_left=%h w_x2y2_right=%h\n", io, jo, lo, mo, w_x1y1_left, w_x1y1_right, w_x1y2_left, w_x1y2_right, w_x2y1_left, w_x2y1_right, w_x2y2_left, w_x2y2_right);
								
								weight = getGradientMagnitude[dx + 1][dy + 1];
								//$fwrite(data_file, "i=%d j=%d l=%d m=%d dx=%d dy=%d weigtht=%h\n", io, jo, lo, mo, dx, dy, weight);
								$fwrite(data_file,"weight:%d\n",weight);
			$fwrite(data_file,"histogram[y1][x1][leftBinIndex]:%d histogram[y1][x1][rightBinIndex]:%d\n",histogram[y1][x1][leftBinIndex],histogram[y1][x1][rightBinIndex]);
			$fwrite(data_file,"histogram[y2][x1][leftBinIndex]:%d histogram[y2][x1][rightBinIndex]:%d\n",histogram[y2][x1][leftBinIndex],histogram[y2][x1][rightBinIndex]);
			$fwrite(data_file,"histogram[y1][x2][leftBinIndex]:%d histogram[y1][x2][rightBinIndex]:%d\n",histogram[y1][x2][leftBinIndex],histogram[y1][x2][rightBinIndex]);
			$fwrite(data_file,"histogram[y2][x2][leftBinIndex]:%d histogram[y2][x2][rightBinIndex]:%d\n",histogram[y2][x2][leftBinIndex],histogram[y2][x2][rightBinIndex]);
								
								$fwrite(data_file,"w_x1y1_left * weight:%d\n",w_x1y1_left * weight);
								$fwrite(data_file,"w_x1y1_right * weight:%d\n",w_x1y1_right * weight);
								$fwrite(data_file,"w_x1y2_left * weight:%d\n",w_x1y2_left * weight);
								$fwrite(data_file,"w_x1y2_right * weight:%d\n",w_x1y2_right * weight);
								$fwrite(data_file,"w_x2y1_left * weight:%d\n",w_x2y1_left * weight);
								$fwrite(data_file,"w_x2y1_right * weight:%d\n",w_x2y1_right * weight);
								$fwrite(data_file,"w_x2y2_left * weight:%d\n",w_x2y2_left * weight);
								$fwrite(data_file,"w_x2y2_right * weight:%d\n",w_x2y2_right * weight);
								
								$fwrite(data_file,"(w_x1y1_left * weight)>>>SHIFT_AMOUNT:%d\n",(w_x1y1_left * weight)>>>SHIFT_AMOUNT);
								$fwrite(data_file,"(w_x1y1_right * weight)>>>SHIFT_AMOUNT:%d\n",(w_x1y1_right * weight)>>>SHIFT_AMOUNT);
								$fwrite(data_file,"(w_x1y2_left * weight)>>>SHIFT_AMOUNT:%d\n",(w_x1y2_left * weight)>>>SHIFT_AMOUNT);
								$fwrite(data_file,"(w_x1y2_right * weight)>>>SHIFT_AMOUNT:%d\n",(w_x1y2_right * weight)>>>SHIFT_AMOUNT);
								$fwrite(data_file,"(w_x2y1_left * weight)>>>SHIFT_AMOUNT:%d\n",(w_x2y1_left * weight)>>>SHIFT_AMOUNT);
								$fwrite(data_file,"(w_x2y1_right * weight)>>>SHIFT_AMOUNT:%d\n",(w_x2y1_right * weight)>>>SHIFT_AMOUNT);
								$fwrite(data_file,"(w_x2y2_left * weight)>>>SHIFT_AMOUNT:%d\n",(w_x2y2_left * weight)>>>SHIFT_AMOUNT);
								$fwrite(data_file,"(w_x2y2_right * weight)>>>SHIFT_AMOUNT:%d\n",(w_x2y2_right * weight)>>>SHIFT_AMOUNT);
								temp = w_x1y1_left * weight;
								temp = temp + ((temp & 1 << (SHIFT_AMOUNT - 1)) << 1);
								temp >>>= SHIFT_AMOUNT;
								histogram[y1][x1][leftBinIndex] += 64'(temp);
								temp = w_x1y1_right * weight;
								temp = temp + ((temp & 1 << (SHIFT_AMOUNT - 1)) << 1);
								temp >>>= SHIFT_AMOUNT;
								histogram[y1][x1][rightBinIndex] += 64'(temp);
								temp = w_x1y2_left * weight;
								temp = temp + ((temp & 1 << (SHIFT_AMOUNT - 1)) << 1);
								temp >>>= SHIFT_AMOUNT;
								histogram[y2][x1][leftBinIndex] += 64'(temp);
								temp = w_x1y2_right * weight;
								temp = temp + ((temp & 1 << (SHIFT_AMOUNT - 1)) << 1);
								temp >>>= SHIFT_AMOUNT;
								histogram[y2][x1][rightBinIndex] += 64'(temp);
								temp = w_x2y1_left * weight;
								temp = temp + ((temp & 1 << (SHIFT_AMOUNT - 1)) << 1);
								temp >>>= SHIFT_AMOUNT;
								histogram[y1][x2][leftBinIndex] += 64'(temp);
								temp = w_x2y1_right * weight;
								temp = temp + ((temp & 1 << (SHIFT_AMOUNT - 1)) << 1);
								temp >>>= SHIFT_AMOUNT;
								histogram[y1][x2][rightBinIndex] += 64'(temp);
								temp = w_x2y2_left * weight;
								temp = temp + ((temp & 1 << (SHIFT_AMOUNT - 1)) << 1);
								temp >>>= SHIFT_AMOUNT;
								histogram[y2][x2][leftBinIndex] += 64'(temp);
								temp = w_x2y2_right * weight;
								temp = temp + ((temp & 1 << (SHIFT_AMOUNT - 1)) << 1);
								temp >>>= SHIFT_AMOUNT;
								histogram[y2][x2][rightBinIndex] += 64'(temp);
			$fwrite(data_file,"histogram[y1][x1][leftBinIndex]:%d histogram[y1][x1][rightBinIndex]:%d\n",histogram[y1][x1][leftBinIndex],histogram[y1][x1][rightBinIndex]);
			$fwrite(data_file,"histogram[y2][x1][leftBinIndex]:%d histogram[y2][x1][rightBinIndex]:%d\n",histogram[y2][x1][leftBinIndex],histogram[y2][x1][rightBinIndex]);
			$fwrite(data_file,"histogram[y1][x2][leftBinIndex]:%d histogram[y1][x2][rightBinIndex]:%d\n",histogram[y1][x2][leftBinIndex],histogram[y1][x2][rightBinIndex]);
			$fwrite(data_file,"histogram[y2][x2][leftBinIndex]:%d histogram[y2][x2][rightBinIndex]:%d\n",histogram[y2][x2][leftBinIndex],histogram[y2][x2][rightBinIndex]);
								
								mo++;
								count = 0;
							end
							if (mo >= jo + 18 || mo >= 128) begin
								lo++;
								mo = jo;
							end
						end
						if (lo >= io + 18 || lo >= 128) begin
							jo += 8'd18;
							lo = io;
							mo = jo;
						end
					end
					if (jo > 128 - 18) begin
						io += 8'd18;
						jo = 0;
						lo = io;
						mo = jo;
					end
				end else begin
					toNormalizedHistogram = 1;
					io = 0;
					jo = 0;
					ko = 0;
					lo = 0;
					mo = 0;
				end
			end else begin
				count++;
			end
		end
		
		if (toNormalizedHistogram) begin
			/* if (idebug < 7) begin
				if (jdebug < 7) begin
					if (kdebug < 9) begin
						out = histogram[idebug][jdebug][kdebug];
//						$fwrite(data_file, "histogram[%d][%d][%d]=%h\n", idebug, jdebug, kdebug, histogram[idebug][jdebug][kdebug]);
						kdebug++;
					end else begin
						jdebug++;
						kdebug=0;
					end
				end else begin
					idebug++;
					jdebug=0;
					kdebug=0;
				end
			end */
			case (step)
/* 				0 : begin
					if (io < 7) begin
						if (jo < 7) begin
							if (ko < 9) begin
								histogram[io][jo][ko] = histogram_out;
//								$fwrite(data_file, "histogram[%d][%d][%d]=%h\n", io, jo, ko, histogram[io][jo][ko]);
							
								ko++;
							end else begin
								jo++;
								ko = 0;
							end
						end else begin
							io++;
							jo = 0;
						end
					end else begin
						step++;
						io = 0;
						jo = 0;
					end
				end */
				
				0 : begin
					if (io < 6) begin
						if (jo < 6) begin
							if (lo < io + 2 && io < 7) begin
								if (mo < jo + 2 && jo < 7) begin
									if (ko < 9) begin
			$fwrite(data_file,"histogram[%d][%d][%d]:%d\n",lo,mo,ko,histogram[lo][mo][ko]);
										temp2 = histogram[lo][mo][ko] * histogram[lo][mo][ko];
						$fwrite(data_file, "histogram[lo][mo][ko] * histogram[lo][mo][ko]:%d\n", temp2);
										//temp2 = histogram[lo][mo][ko];
										//temp2 = temp2 * temp2;
										temp2 = temp2 + ((temp2 & 1 << (SHIFT_AMOUNT - 1)) << 1);
						$fwrite(data_file, "temp2 + ((temp2 & 1 << (SHIFT_AMOUNT - 1)) << 1):%d\n", temp2);
										temp2 >>>= SHIFT_AMOUNT;
						$fwrite(data_file, "temp2 >>>= SHIFT_AMOUNT:%d\n", temp2);
										acumulator += temp2;
										
						$fwrite(data_file, "acumulator:%d\n", acumulator);
										//$fwrite(data_file, "i=%d j=%d l=%d m=%d k=%d acumulator=%h\n", io, jo, lo, mo, ko, acumulator);
										
										ko++;
									end else begin
										mo++;
										ko = 0;
									end
								end else begin
									lo++;
									mo = jo;
									ko = 0;
								end
							end else begin
								//jo++;
								lo = io;
								mo = jo;
								ko = 0;			
								//acumulator = 128'h1400000;
								in = acumulator;		
								step++;
						$fwrite(data_file, "acumulator:%d\n", acumulator);
								//$fwrite(data_file, "acumulator=%h\n", acumulator);
							end
						end else begin
							io++;
							jo = 0;
							lo = io;
							mo = mo;
							ko = 0;
						end
					end else begin
						//featuresCounter = 0;
						//step = 4;
						//$fwrite(data_file, "end\n");
				$fwrite(data_file, "end\n");
				
				$fclose(data_file);
				$stop;
				/* 		if (end_signal == 0) begin
							end_signal = 1;
							//$fwrite(data_file, "end\n");
							//$stop;
							//$fclose(data_file);
						end */
					end
				end
				
				1 : begin
					e = out;
					
					if (e < 0) begin
						aux2 = acumulator << $unsigned(e * -1);
					end else begin
						aux2 = acumulator >> $unsigned(e);
					end
					e = 8'(e + 127);
					//$fwrite(data_file, "e=%h aux=%h\n", e, aux2);
					temp2 = {1'b0,8'(e),23'(aux2 >> (SHIFT_AMOUNT - 23))};
					//$fwrite(data_file, "temp=%h\n", temp2);
					
					temp2 = 128'h5F3759DF - (temp2 >> 1);
					//$fwrite(data_file, "result=%h\n", temp2);   
					
					e = 8'((temp2 >> 23) - 127);
					//$fwrite(data_file, "e=%h\n", e); 
					aux2 = (1 << SHIFT_AMOUNT) + (23'(temp2) << (SHIFT_AMOUNT - 23));
					if (e < 0) begin
						temp2 = aux2 >> $unsigned(e * -1);
					end else begin
						temp2 = aux2 << $unsigned(e);
					end
					//$fwrite(data_file, "aux=%h temp=%h\n", aux2, temp2); 
					
					//y  = y * ( threehalfs - ( x2 * y * y ) )
					//y * y
					aux2 = temp2 * temp2;
					aux2 = aux2 + ((aux2 & 1 << (SHIFT_AMOUNT - 1)) << 1);
					aux2 >>>= SHIFT_AMOUNT;
					//number * y * y
					aux2 = aux2 * acumulator;
					aux2 = aux2 + ((aux2 & 1 << (SHIFT_AMOUNT - 1)) << 1);
					aux2 >>>= SHIFT_AMOUNT;
					//number * 0.5 * y * y
					aux2 = aux2 * 128'h4000000;
					aux2 = aux2 + ((aux2 & 1 << (SHIFT_AMOUNT - 1)) << 1);
					aux2 >>>= SHIFT_AMOUNT;
					//threehalfs - ( x2 * y * y )
					aux2 = 128'hc000000 - aux2;
					//y  = y * ( threehalfs - ( x2 * y * y ) )
					temp2 = temp2 * aux2;
					temp2 = temp2 + ((temp2 & 1 << (SHIFT_AMOUNT - 1)) << 1);
					temp2 >>>= SHIFT_AMOUNT;
					
					//y  = y * ( threehalfs - ( x2 * y * y ) )
					//y * y
					aux2 = temp2 * temp2;
					aux2 = aux2 + ((aux2 & 1 << (SHIFT_AMOUNT - 1)) << 1);
					aux2 >>>= SHIFT_AMOUNT;
					//number * y * y
					aux2 = aux2 * acumulator;
					aux2 = aux2 + ((aux2 & 1 << (SHIFT_AMOUNT - 1)) << 1);
					aux2 >>>= SHIFT_AMOUNT;
					//number * 0.5 * y * y
					aux2 = aux2 * 128'h4000000;
					aux2 = aux2 + ((aux2 & 1 << (SHIFT_AMOUNT - 1)) << 1);
					aux2 >>>= SHIFT_AMOUNT;
					//threehalfs - ( x2 * y * y )
					aux2 = 128'hc000000 - aux2;
					//y  = y * ( threehalfs - ( x2 * y * y ) )
					temp2 = temp2 * aux2;
					temp2 = temp2 + ((temp2 & 1 << (SHIFT_AMOUNT - 1)) << 1);
					temp2 >>>= SHIFT_AMOUNT;
					
					//y  = y * ( threehalfs - ( x2 * y * y ) )
					//y * y
					aux2 = temp2 * temp2;
					aux2 = aux2 + ((aux2 & 1 << (SHIFT_AMOUNT - 1)) << 1);
					aux2 >>>= SHIFT_AMOUNT;
					//number * y * y
					aux2 = aux2 * acumulator;
					aux2 = aux2 + ((aux2 & 1 << (SHIFT_AMOUNT - 1)) << 1);
					aux2 >>>= SHIFT_AMOUNT;
					//number * 0.5 * y * y
					aux2 = aux2 * 128'h4000000;
					aux2 = aux2 + ((aux2 & 1 << (SHIFT_AMOUNT - 1)) << 1);
					aux2 >>>= SHIFT_AMOUNT;
					//threehalfs - ( x2 * y * y )
					aux2 = 128'hc000000 - aux2;
					//y  = y * ( threehalfs - ( x2 * y * y ) )
					temp2 = temp2 * aux2;
					temp2 = temp2 + ((temp2 & 1 << (SHIFT_AMOUNT - 1)) << 1);
					temp2 >>>= SHIFT_AMOUNT;
					
					temp2 = temp2 * acumulator;
					temp2 = temp2 + ((temp2 & 1 << (SHIFT_AMOUNT - 1)) << 1);
					temp2 >>>= SHIFT_AMOUNT;
					sqrt_acumulator = temp2;
					//$fwrite(data_file, "sqrt_acumulator2=%h\n", sqrt_acumulator);
		$fwrite(data_file, "sqrt_acumulator:%d\n", sqrt_acumulator);
					step++;
				end	
				
				2 : begin
					if (lo < io + 2 && io < 7) begin
						if (mo < jo + 2 && jo < 7) begin
							if (ko < 9) begin
								aux2 = (histogram[lo][mo][ko] << SHIFT_AMOUNT) / sqrt_acumulator;
								//aux2 = histogram[lo][mo][ko] * sqrt_acumulator;
								//aux2 = aux2 + ((aux2 & 1 << (SHIFT_AMOUNT - 1)) << 1);
								//aux2 >>>= SHIFT_AMOUNT;
								//$fwrite(data_file, "aux=%h\n", aux2);
								//featuresVector[featuresCounter] = 28'(aux2);
								if (aux2 > 128'h1999999) begin
									//featuresVector[featuresCounter] = 28'h1999999;
									aux2 = 128'h1999999;
								end
								featuresVector_out = aux2;
								
								if (end_signal == 0) begin
									end_signal = 1;
								end
						$fwrite(data_file, "%h\n", featuresVector_out);
								
								//$fwrite(data_file, "i=%d j=%d k=%d histogram=%h featuresVector=%h sqrt_acumulator=%h\n", lo, mo, ko, 128'(histogram[lo][mo][ko]), aux2, sqrt_acumulator);
								//$fwrite(data_file, "i=%d j=%d k=%d featuresVector=%h\n", lo, mo, ko, featuresVector[featuresCounter]);
								//featuresCounter++;
								
								ko++;
							end else begin
								mo++;
								ko = 0;
							end
						end else begin
							lo++;
							mo = jo;
							ko = 0;
						end
					end else begin
						jo++;
						lo = io;
						mo = jo;
						ko = 0;
						acumulator = 0;
						step = 0;
					end
				end
				
				// 4 : begin
					// if (featuresCounter < 6*6*9*2*2) begin
						// featuresVector_out = 128'(featuresVector[featuresCounter]);
						// $fwrite(data_file, "i=%d featuresVector_out=%h\n", featuresCounter, featuresVector[featuresCounter]);
						
						// featuresCounter++;
					// end else begin
						// end_signal = 1;
					// end
				// end
			endcase
		end
	end

endmodule

module highbit #(
		parameter OUT_WIDTH = 8, // out uses one extra bit for not-found
									 
		parameter IN_WIDTH = 1<<(OUT_WIDTH-1)
) (
		input [IN_WIDTH-1:0]in,
		output [OUT_WIDTH-1:0]out
);

wire [OUT_WIDTH-1:0]out_stage[0:IN_WIDTH];
assign out_stage[0] = ~8'b0; // desired default output if no bits set
genvar i;
generate
	for(i=0; i<IN_WIDTH; i=i+1) begin : high_bit_finder
		assign out_stage[i+1] = in[i] ? 8'(i - 27) : out_stage[i]; 
	end
endgenerate
assign out = out_stage[IN_WIDTH];

endmodule