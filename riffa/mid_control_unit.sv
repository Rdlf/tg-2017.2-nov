module mid_control_unit (
		input clk,
		input fifo_mid_is_empty,
		input bit [31:0] captured_data,
		input bit rst,
		output bit start,
		output bit toOtsuThreshold,
		output bit toHistogramBins,
		output bit end_signal,
		output bit [31:0] featuresVector_out
	);
	// integer data_file;
	// integer scan_file;
	//logic signed [31:0] captured_data;
	
	bit [11:0] address_a, address_b, address_a1, address_b1, address_a2, address_b2;
	int data_a, data_b, q_a, q_b;
	bit wren_a, wren_b;
	
	//bit rst, start, toOtsuThreshold, toHistogramBins, end_signal;
	
	bit [31:0] pixels;
	bit [127:0] threshold;
	int count;
	
	//bit [63:0] histogram_out;
	
	//ram
	ram2port32bits ram(.address_a(address_a), .address_b(address_b), .clock(clk), .data_a(data_a), .data_b(data_b), .wren_a(wren_a), .wren_b(wren_b), .q_a(q_a), .q_b(q_b));
	//otsu
	histogramGrayScale histogramGrayScale(.clk(clk), .rst(rst), .start(start), .in(pixels), .out(threshold), .toOtsuThreshold(toOtsuThreshold));
	//otsuLevel otsuLevel(.clk(clk), .rst(rst), .start(toOtsuLevel), .in(histogram), .out(threshold), .toOtsuThreshold(toOtsuThreshold));
	otsuThreshold otsuThreshold(.clk(clk), .rst(rst), .start(start), .in(pixels), .threshold(threshold), .toOtsuThreshold(toOtsuThreshold), .q_a(q_a), .address_a(address_a1), .address_b(address_b1), .data_b(data_b), .wren_b(wren_b), .toHistogramBins(toHistogramBins));
	//hog
	histogramBins histogramBins(
		.clk(clk), 
		.rst(rst), 
		.start(toHistogramBins), 
		.q_a(q_a), 
		.q_b(q_b), 
		.address_a(address_a2), 
		.address_b(address_b2), 
		.featuresVector_out(featuresVector_out), 
		.end_signal(end_signal)
	);
	// normalizedHistogram normalizedHistogram(
		// .clk(clk), 
		// .rst(rst), 
		// .start(toNormalizedHistogram), 
		// .histogram_out(histogram_out), 
		// .featuresVector_out(featuresVector_out), 
		// .end_signal(end_signal)
	// );
	
	initial begin
		count <= 0;
		start <= 0;
		
		// data_file = $fopen("mid_control_unit.txt", "w");
		// if (data_file == 0) begin
			// $display("data_file handle was NULL");
			// $finish;
		// end
	end
	
	always_ff @(negedge clk) begin
		if (toHistogramBins == 0) begin
			address_a <= address_a1;
			address_b <= address_b1;
		end else begin
			address_a <= address_a2;
			address_b <= address_b2;
		end
	end
	
	always_ff @(posedge clk) begin
		if (start == 1) begin
			start <= 0;
		end
				
		if (rst) begin
			count <= 0;
			//rst = 0;
		end else begin
			if (count < 4096 & fifo_mid_is_empty == 0) begin
				//scan_file = $fscanf(data_file, "%h\r\n", captured_data);
				//pixels = captured_data;
				
				//if ((start == 0) & (count > 0) & (count % 4 == 3)) begin
					start <= 1;
				//end
				
				//pixels = pixels | 32'(captured_data << (8 * (3 - (count % 4))));
				pixels <= {captured_data[7:0], captured_data[15:8], captured_data[23:16], captured_data[31:24]};
				//$fwrite(data_file, "%h\n", pixels);
				
				count <= count + 1;
			end 

			
			// if(end_signal) begin
				// rst = 1;
				// start = 0;
			// end
		end
	end
	
	/* always @(posedge clk) begin
		if (rst) begin
			count = 0;
			rst = 0;
		end else begin
			if (count <= 4096) begin
				scan_file = $fscanf(data_file, "%h\r\n", captured_data);
				pixels = captured_data;
				
				if (start == 0) begin
					start = 1;
				end
				count++;
			end 

			
			if(end_signal) begin
				rst = 1;
				start = 0;
			end
		end
	end */
endmodule