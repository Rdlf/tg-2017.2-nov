/*
#  g++  -o teste ocr.c `pkg-config --cflags --libs opencv`
*/

#include <stdlib.h>
#include <cmath>
#include <time.h>
#include <cstring>
#include <stdio.h>
#include <omp.h>
#include "image.h"
#include "imageprocessing.h"
#include "elm.hpp"
#define R128_IMPLEMENTATION
#include "r128.h"
#define R128_ASSERT


using namespace std;
void destroyRGBImg(RGBImg *img)
{
	unsigned int i;
	for (i = 0; i < img->rows; i++)
	{
		free(img->pixels[i]);
	}
	free(img->pixels);
	free(img);
}

void destroyGRAYorBINImg(BINImg *img)
{
	unsigned int i;
	for (i = 0; i < img->rows; i++)
	{
		free(img->pixels[i]);
	}
	free(img->pixels);
	free(img);
}

void destroyHistogramR128(HistogramR128 ** src, int rows)
{
	int i, j;
	for (i = 0; i < rows; i++)
	{
		for (j = 0; j < rows; j++)
			free(src[i][j].values);

		free(src[i]);
	}
	free(src);
}

void destroyHistogram(Histogram ** src, int rows)
{
	int i, j;
	for (i = 0; i < rows; i++)
	{
		for (j = 0; j < rows; j++)
			free(src[i][j].values);

		free(src[i]);
	}
	free(src);
}

void destroyGrad(Gradient *src)
{
	int i;
	for (i = 0; i < src->rows; i++)
	{
		free(src->values[i]);
	}
	free(src->values);
	free(src);

}
vector< vector< vector< double > * > * > *dataForElm = new vector< vector< vector< double > * > * >;

vector< vector< vector< double > * > * > *dataForElm_test = new vector< vector< vector< double > * > * >;


int main()
{
	int qtde_treino = 930/*6113*/;
	int qtde_teste = 930/*5379*/;
	FILE *treino = fopen("treino_chars75k_15_2.txt", "r");
	FILE *teste = fopen("teste_chars75k_15_2.txt", "r");

	FILE *hog_sv = fopen("featuresVector_double.txt", "r");
	long double hog_o = 0;
	char end[6];

	//FILE *treino_novo = fopen("training_features.txt","w");
	//FILE *teste_novo = fopen("testing_features.txt","w");

	/*EXPERIMENTS*/

	double time_rgb = 0;
	double time_resize = 0;
	double time_otsu = 0;
	double time_hog = 0;

	/*Extreme Learning Machine*/
	int numberOfFeatures = 1296;
	int numberOfHiddenNeurons = 50000;
	int numberOfLabel = 62;
	int numberOfImages = qtde_treino;
	double epsilon = 0;
	double eta = 1;
	int activationFunctionValue = 1;

	/*End of Extreme Learning Machine*/


	/*RIFFA
	fpga_t * fpga;
	fpga = fpga_open(0);
	fpga_reset(fpga);


	/*END RIFFA*/




	for (int k = 0; k < qtde_treino + qtde_teste; k++) {

		char path2[120];
		int label = 0;
		//  fpga_reset(fpga);

		if (k < qtde_treino) {
			int iu = fscanf(treino, " %s %d", path2, &label);
		}
		else {
			int iu = fscanf(teste, " %s %d", path2, &label);
		}
		//int iu = fscanf(teste," %[^\n]s ",path2);
		if (k % 100 == 0)
		{
			printf("%d\n", k);
		}

		if (k == qtde_treino)
		{
			printf("Acabei o treino\n");
			// fclose(treino_novo);        
		}

		//printf("%s\n",path2 );
		//getchar();

		char *name = strcat(path2, "");

		cv::Mat img = cv::imread(name, 1);


		RGBImg *minhaImagem = createRGBImg(img.rows, img.cols);
		GRAYImg *imagemGray = createGRAYImg(img.rows, img.cols);
		BINImg *imagemThresh;


		matToRGBImg(minhaImagem, img);
		//delete img;

		clock_t res = clock();
		rgb2gray(imagemGray, minhaImagem);
		res = clock() - res;
		time_rgb += ((float)res) / CLOCKS_PER_SEC;
		// if(k+10 >= qtde_treino)
		  //   printf("rgb to gray took: %g\n", ((float)res)/CLOCKS_PER_SEC);  

		destroyRGBImg(minhaImagem);

		res = clock();
		GRAYImg *scaled = resize(imagemGray, 128, 128);
		// GRAYImg *scaled = resizeFPGA(imagemGray, fpga);
		res = clock() - res;
		time_resize += ((float)res) / CLOCKS_PER_SEC;
		//if(k+10 >= qtde_treino)
		 //   printf("resize took: %g\n", ((float)res)/CLOCKS_PER_SEC);



		destroyGRAYorBINImg((BINImg*)imagemGray);
		res = clock();
		imagemThresh = otsuThreshold(scaled);
		res = clock() - res;
		time_otsu += ((float)res) / CLOCKS_PER_SEC;
		// if(k+10 >= qtde_treino)
		  //   printf("otsu took: %g\n", ((float)res)/CLOCKS_PER_SEC);

		destroyGRAYorBINImg((BINImg*)scaled);
		res = clock();

		Gradient *grad = imageGradient(imagemThresh);

		destroyGRAYorBINImg(imagemThresh);

		Histogram ** histograma = histogramBins(grad, 9, 18);
		double *hog = normalizedHistogram(histograma, 9, 2, 18, grad);
		//HistogramR128 ** histograma = histogramBinsR128(grad, 9, 18);
		//R128 *hog = normalizedHistogramR128(histograma, 9, 2, 18, grad);

		res = clock() - res;
		time_hog += ((float)res) / CLOCKS_PER_SEC;
		// if(k+10 >= qtde_treino)
		//     printf("hog took: %g\n\n\n", ((float)res)/CLOCKS_PER_SEC);
		//destroyHistogramR128(histograma, grad->rows / 18);
		destroyHistogram(histograma, grad->rows / 18);
		destroyGrad(grad);

		if (k < qtde_treino)
		{
			dataForElm->push_back(new vector <vector <double>*>);
			dataForElm->at(k)->push_back(new vector <double>()); //alocando
			dataForElm->at(k)->push_back(new vector <double>());

		}
		else
		{
			dataForElm_test->push_back(new vector <vector <double>*>);
			dataForElm_test->at(k - qtde_treino)->push_back(new vector <double>()); //alocando
			dataForElm_test->at(k - qtde_treino)->push_back(new vector <double>());
		}
		// numberOfFeatures = 1196 - 100;
		for (int o = 0; o < 1296; o++)
		{
			fscanf(hog_sv, "%Lf", &hog_o);
			if (k < qtde_treino)
			{
				//printf("%g \n", hog[o]);
				//getchar();
				//dataForElm->at(k)->at(0)->push_back(hog[o]);
				dataForElm->at(k)->at(0)->push_back(hog_o);
				//     fprintf(treino_novo, "%.15lf ", hog[o]);

			}
			else
			{
				//dataForElm_test->at(k - qtde_treino)->at(0)->push_back(hog[o]);
				dataForElm_test->at(k - qtde_treino)->at(0)->push_back(hog_o);
				//    fprintf(teste_novo, "%.15lf ", hog[o]);
			}
		}
		fscanf(hog_sv, "%s", end);


		if (k < qtde_treino)
		{
			//    fprintf(treino_novo, "%d\n", label);
			for (unsigned int labelIndex = 0; labelIndex < numberOfLabel; labelIndex++)
			{
				dataForElm->at(k)->at(1)->push_back(0);
			}
			dataForElm->at(k)->at(1)->at(/*k/15*/ label /*label*/) = 1;
		}
		else
		{
			//   fprintf(teste_novo, "%d\n",label);
			for (unsigned int labelIndex = 0; labelIndex < numberOfLabel; labelIndex++)
			{
				dataForElm_test->at(k - qtde_treino)->at(1)->push_back(0);
			}
			dataForElm_test->at(k - qtde_treino)->at(1)->at(/*(k-qtde_treino)/15*/ label /*label*/) = 1;
		}
		free(hog);
	}
	//printf("time img processing: %lf\n");

	fclose(treino);
	fclose(teste);
	//fpga_close(fpga);


	printf("RGB: %lf\n", time_rgb);
	printf("RESIZE: %lf\n", time_resize);
	printf("OTSU: %lf\n", time_otsu);
	printf("HOG: %lf\n", time_hog);
	//FILE *result = fopen("simulacao_elm.txt","w");
	int benchmark = 0;
	for (numberOfHiddenNeurons = 1000; 1; numberOfHiddenNeurons += 1000)
	{
		activationFunctionValue = 2;
		Elm* elmObj = new Elm(numberOfFeatures, numberOfHiddenNeurons, numberOfLabel, numberOfImages, epsilon, eta, activationFunctionValue);
		elmObj->trainning(0, 0, dataForElm);
		// printf("training\n");
		clock_t t = clock();
		double elmErrorRate = elmObj->recognitionRate(dataForElm_test);
		t = clock() - t;
		printf("%d %lf, time: %g\n", numberOfHiddenNeurons, (elmErrorRate) * 100, ((float)t) / CLOCKS_PER_SEC);
		//fprintf(result,"%d %lf\n",numberOfHiddenNeurons, (elmErrorRate)*100);
		delete elmObj;
	}
	return 0;
}

