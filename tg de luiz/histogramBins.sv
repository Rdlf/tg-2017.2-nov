module histogramBins (
		input clk,
		input rst,
		input start,
		input byte unsigned q_a,
		input byte unsigned q_b,
		output bit [13:0] address_a = 0,
		output bit [13:0] address_b = 0,
		output bit [64:0] featuresVector_out,
		output bit end_signal = 0
	);
	parameter SHIFT_AMOUNT = 27;
	parameter SHIFT_MASK = (1 << SHIFT_AMOUNT) - 1;
	
	bit signed [127:0] temp, aux, angle, rightRatio, leftRatio, w_x1y1_left, w_x1y1_right, w_x1y2_left, w_x1y2_right, w_x2y1_left, w_x2y1_right, w_x2y2_left, w_x2y2_right, weight, acumulator = 0, sqrt_acumulator;
	bit [127:0] temp2, aux2;
	bit signed [127:0] atan2[3][3] = '{'{128'h6487ed4, 128'h1921fb54, 128'h12d97c7e}, '{128'hc90fda9, 128'h0, 128'hc90fdaa}, '{128'h12d97c7e, 128'h0, 128'h6487ed4}}, getGradientMagnitude[3][3] = '{'{128'hb504f33, 128'h8000000, 128'hb504f33}, '{128'h8000000, 128'h0, 128'h8000000}, '{128'hb504f33, 128'h8000000, 128'hb504f33}};
	bit signed [63:0] histogram[7][7][9] = '{'{'{0,0,0,0,0,0,0,0,0},'{0,0,0,0,0,0,0,0,0},'{0,0,0,0,0,0,0,0,0},'{0,0,0,0,0,0,0,0,0},'{0,0,0,0,0,0,0,0,0},'{0,0,0,0,0,0,0,0,0},'{0,0,0,0,0,0,0,0,0}},'{'{0,0,0,0,0,0,0,0,0},'{0,0,0,0,0,0,0,0,0},'{0,0,0,0,0,0,0,0,0},'{0,0,0,0,0,0,0,0,0},'{0,0,0,0,0,0,0,0,0},'{0,0,0,0,0,0,0,0,0},'{0,0,0,0,0,0,0,0,0}},'{'{0,0,0,0,0,0,0,0,0},'{0,0,0,0,0,0,0,0,0},'{0,0,0,0,0,0,0,0,0},'{0,0,0,0,0,0,0,0,0},'{0,0,0,0,0,0,0,0,0},'{0,0,0,0,0,0,0,0,0},'{0,0,0,0,0,0,0,0,0}},'{'{0,0,0,0,0,0,0,0,0},'{0,0,0,0,0,0,0,0,0},'{0,0,0,0,0,0,0,0,0},'{0,0,0,0,0,0,0,0,0},'{0,0,0,0,0,0,0,0,0},'{0,0,0,0,0,0,0,0,0},'{0,0,0,0,0,0,0,0,0}},'{'{0,0,0,0,0,0,0,0,0},'{0,0,0,0,0,0,0,0,0},'{0,0,0,0,0,0,0,0,0},'{0,0,0,0,0,0,0,0,0},'{0,0,0,0,0,0,0,0,0},'{0,0,0,0,0,0,0,0,0},'{0,0,0,0,0,0,0,0,0}},'{'{0,0,0,0,0,0,0,0,0},'{0,0,0,0,0,0,0,0,0},'{0,0,0,0,0,0,0,0,0},'{0,0,0,0,0,0,0,0,0},'{0,0,0,0,0,0,0,0,0},'{0,0,0,0,0,0,0,0,0},'{0,0,0,0,0,0,0,0,0}},'{'{0,0,0,0,0,0,0,0,0},'{0,0,0,0,0,0,0,0,0},'{0,0,0,0,0,0,0,0,0},'{0,0,0,0,0,0,0,0,0},'{0,0,0,0,0,0,0,0,0},'{0,0,0,0,0,0,0,0,0},'{0,0,0,0,0,0,0,0,0}}}; //histogram = 4mb
	shortint leftBinIndex, rightBinIndex, x1, y1, x2, y2;
	bit signed [15:0] histogramMap[9] = '{16'd10, 16'd30, 16'd50, 16'd70, 16'd90, 16'd110, 16'd130, 16'd150, 16'd170};
	byte io = 0, jo = 0, lo = 0, mo = 0, dx, dy, ko = 0, state = 0, e;
	bit toNormalizedHistogram = 0;
	
	bit [7:0] out;
	bit [127:0] in;
	
	highbit highbit(.in(in), .out(out));
  
	integer data_file;
	integer scan_file;

	initial begin
		data_file = $fopen("featuresVector.txt", "w");
		if (data_file == 0) begin
			$display("data_file handle was NULL");
			$finish;
		end
	end
	
	always_ff @(posedge clk) begin
		if (rst) begin
			histogram <= '{'{'{0,0,0,0,0,0,0,0,0},'{0,0,0,0,0,0,0,0,0},'{0,0,0,0,0,0,0,0,0},'{0,0,0,0,0,0,0,0,0},'{0,0,0,0,0,0,0,0,0},'{0,0,0,0,0,0,0,0,0},'{0,0,0,0,0,0,0,0,0}},'{'{0,0,0,0,0,0,0,0,0},'{0,0,0,0,0,0,0,0,0},'{0,0,0,0,0,0,0,0,0},'{0,0,0,0,0,0,0,0,0},'{0,0,0,0,0,0,0,0,0},'{0,0,0,0,0,0,0,0,0},'{0,0,0,0,0,0,0,0,0}},'{'{0,0,0,0,0,0,0,0,0},'{0,0,0,0,0,0,0,0,0},'{0,0,0,0,0,0,0,0,0},'{0,0,0,0,0,0,0,0,0},'{0,0,0,0,0,0,0,0,0},'{0,0,0,0,0,0,0,0,0},'{0,0,0,0,0,0,0,0,0}},'{'{0,0,0,0,0,0,0,0,0},'{0,0,0,0,0,0,0,0,0},'{0,0,0,0,0,0,0,0,0},'{0,0,0,0,0,0,0,0,0},'{0,0,0,0,0,0,0,0,0},'{0,0,0,0,0,0,0,0,0},'{0,0,0,0,0,0,0,0,0}},'{'{0,0,0,0,0,0,0,0,0},'{0,0,0,0,0,0,0,0,0},'{0,0,0,0,0,0,0,0,0},'{0,0,0,0,0,0,0,0,0},'{0,0,0,0,0,0,0,0,0},'{0,0,0,0,0,0,0,0,0},'{0,0,0,0,0,0,0,0,0}},'{'{0,0,0,0,0,0,0,0,0},'{0,0,0,0,0,0,0,0,0},'{0,0,0,0,0,0,0,0,0},'{0,0,0,0,0,0,0,0,0},'{0,0,0,0,0,0,0,0,0},'{0,0,0,0,0,0,0,0,0},'{0,0,0,0,0,0,0,0,0}},'{'{0,0,0,0,0,0,0,0,0},'{0,0,0,0,0,0,0,0,0},'{0,0,0,0,0,0,0,0,0},'{0,0,0,0,0,0,0,0,0},'{0,0,0,0,0,0,0,0,0},'{0,0,0,0,0,0,0,0,0},'{0,0,0,0,0,0,0,0,0}}};
			io <= 0;
			jo <= 0;
					
			lo <= 0;
			mo <= 0;
			address_a <= 0;
			address_b <= 0;
			toNormalizedHistogram <= 0;
			ko <= 0;
			state <= 0;
			acumulator <= 0;
			end_signal <= 0;
		end
		
		case (state)
		0: begin
			if (start) begin
				if (lo == 0) begin
					address_a <= 14'(lo * 128 + mo);
					address_b <= 14'(lo * 128 + mo);
				end else if (lo == 128 - 1) begin
					address_a <= 14'(lo * 128 + mo);
					address_b <= 14'((lo - 1) * 128 + mo);
				end else begin
					address_a <= 14'((lo + 1) * 128 + mo);
					address_b <= 14'((lo - 1) * 128 + mo);
				end
				
				state <= state + 1;
			end
		end
		
		1: begin
			$fwrite(data_file,"lo:%d mo:%d\n",lo,mo);
			$fwrite(data_file, "address_a=%d address_b=%d ", address_a, address_b);
			state <= state + 1;
		end
		
		2: begin
			state <= state + 1;
		end
		
		3: begin
			$fwrite(data_file, "q_a=%d q_b=%d\n", q_a, q_b);
			if (lo == 0) begin
				dx <= 0;
			end else begin
				dx <= q_a - q_b;
			end
			
			if (mo == 0) begin
				address_a <= 14'(lo * 128 + mo);
				address_b <= 14'(lo * 128 + mo);
			end else if (mo == 128 - 1) begin
				address_a <= 14'(lo * 128 + mo);
				address_b <= 14'(lo * 128 + (mo - 1));
			end else begin
				address_a <= 14'(lo * 128 + (mo + 1));
				address_b <= 14'(lo * 128 + (mo - 1));
			end
				
			state <= state + 1;
		end
		
		4: begin
			$fwrite(data_file,"lo:%d mo:%d\n",lo,mo);
			$fwrite(data_file, "address_a=%d address_b=%d ", address_a, address_b);
			state <= state + 1;
		end
		
		5: begin
			state <= state + 1;
		end
		
		6: begin
			$fwrite(data_file, "q_a=%d q_b=%d\n", q_a, q_b);
			if (mo == 0) begin
				dy <= 0;
			end else begin
				dy <= q_a - q_b;
			end
				
			state <= state + 1;
		end
		
		7: begin
			$fwrite(data_file,"dx:%d dy:%d\n",dx,dy);
			if (io <= 128 - 18) begin
				if (jo <= 128 - 18) begin
					if (lo < io + 18 && lo < 128) begin
						if (mo < jo + 18 && mo < 128) begin
							angle <= (atan2[dx + 1][dy + 1] * 127'h1ca5dc1af) >>> SHIFT_AMOUNT;
							weight <= getGradientMagnitude[dx + 1][dy + 1];
							
							state <= state + 1;
						end
					end
				end
			end else begin
				state <= 15;
				io <= 0;
				jo <= 0;
				ko <= 0;
				lo <= 0;
				mo <= 0;
			end
		end
		
		8: begin
			if ((((((angle << SHIFT_AMOUNT) / (20 << SHIFT_AMOUNT)) + (1 << (SHIFT_AMOUNT - 1))) >>> SHIFT_AMOUNT) - 1) == -1) begin
				leftBinIndex <= 16'(9 - 1);
			end else begin
				leftBinIndex <= ((((angle << SHIFT_AMOUNT) / (20 << SHIFT_AMOUNT)) + (1 << (SHIFT_AMOUNT - 1))) >>> SHIFT_AMOUNT) - 1;
			end
			
			if (((((angle << SHIFT_AMOUNT) / (20 << SHIFT_AMOUNT)) + (1 << (SHIFT_AMOUNT - 1))) >>> SHIFT_AMOUNT) == 9) begin
				rightBinIndex <= 0;
			end else begin
				rightBinIndex <= (((angle << SHIFT_AMOUNT) / (20 << SHIFT_AMOUNT)) + (1 << (SHIFT_AMOUNT - 1))) >>> SHIFT_AMOUNT;
			end
			
			state <= state + 1;
		end
		
		9: begin
			$fwrite(data_file,"leftBinIndex:%d rightBinIndex:%d\n",leftBinIndex,rightBinIndex);
			if (leftBinIndex == (9 - 1)) begin
				leftRatio <= 128'h4000000;
				rightRatio <= 128'h4000000;
			end else begin
				if (((histogramMap[leftBinIndex] << SHIFT_AMOUNT) - angle) < 0) begin
					rightRatio <= ((((histogramMap[leftBinIndex] << SHIFT_AMOUNT) - angle) * -1) << SHIFT_AMOUNT) / (20 << SHIFT_AMOUNT);
					leftRatio <= (1 << SHIFT_AMOUNT) - (((((histogramMap[leftBinIndex] << SHIFT_AMOUNT) - angle) * -1) << SHIFT_AMOUNT) / (20 << SHIFT_AMOUNT));
				end else begin
					rightRatio <= ((((histogramMap[leftBinIndex] << SHIFT_AMOUNT) - angle) << SHIFT_AMOUNT) << SHIFT_AMOUNT) / (20 << SHIFT_AMOUNT);
					leftRatio <= (1 << SHIFT_AMOUNT) - (((((histogramMap[leftBinIndex] << SHIFT_AMOUNT) - angle) << SHIFT_AMOUNT) << SHIFT_AMOUNT) / (20 << SHIFT_AMOUNT));
				end
			end
			
			x1 <= 16'(mo / 18);
			y1 <= 16'(lo / 18);
			
			// if (((angle << SHIFT_AMOUNT) / (20 << SHIFT_AMOUNT)) >>> SHIFT_AMOUNT == 9) begin
				// z1 <= 16'((((angle << SHIFT_AMOUNT) / (20 << SHIFT_AMOUNT)) >>> SHIFT_AMOUNT) - 1);
			// end else begin
				// z1 <= 16'(((angle << SHIFT_AMOUNT) / (20 << SHIFT_AMOUNT)) >>> SHIFT_AMOUNT);
			// end
			
			state <= state + 1;
		end
		
		10: begin
			$fwrite(data_file,"rightRatio:%d leftRatio:%d\n",rightRatio,leftRatio);
			$fwrite(data_file,"x1:%d y1:%d\n",x1,y1);
			if (x1 + 1 == 128 / 18) begin
				x2 <= x1;
			end else begin
				x2 <= 16'(x1 + 1);
			end
			
			if (y1 + 1 == 128 / 18) begin
				y2 <= y1;
			end else begin
				y2 <= 16'(y1 + 1);
			end
			
			// if (z1 + 1 == 9) begin
				// z2 <= z1;
			// end else begin
				// z2 <= 16'(z1 + 1);
			// end
			
			if (((((((mo - x1) / 18) - 1) * (((lo - y1) / 18) - 1)) << SHIFT_AMOUNT) * leftRatio) >>> SHIFT_AMOUNT < 0) begin
				w_x1y1_left <= (((((((mo - x1) / 18) - 1) * (((lo - y1) / 18) - 1)) << SHIFT_AMOUNT) * leftRatio) >>> SHIFT_AMOUNT) * -1;
			end else begin
				w_x1y1_left <= ((((((mo - x1) / 18) - 1) * (((lo - y1) / 18) - 1)) << SHIFT_AMOUNT) * leftRatio) >>> SHIFT_AMOUNT;
			end
			
			if (((((((mo - x1) / 18) - 1) * (((lo - y1) / 18) - 1)) << SHIFT_AMOUNT) * rightRatio) >>> SHIFT_AMOUNT < 0) begin
				w_x1y1_right <= (((((((mo - x1) / 18) - 1) * (((lo - y1) / 18) - 1)) << SHIFT_AMOUNT) * rightRatio) >>> SHIFT_AMOUNT) * -1;
			end else begin
				w_x1y1_right <= ((((((mo - x1) / 18) - 1) * (((lo - y1) / 18) - 1)) << SHIFT_AMOUNT) * rightRatio) >>> SHIFT_AMOUNT;
			end
			
			if (((((((mo - x1) / 18) - 1) * ((lo - y1) / 18)) << SHIFT_AMOUNT) * leftRatio) >>> SHIFT_AMOUNT < 0) begin
				w_x1y2_left <= (((((((mo - x1) / 18) - 1) * ((lo - y1) / 18)) << SHIFT_AMOUNT) * leftRatio) >>> SHIFT_AMOUNT) * -1;
			end else begin
				w_x1y2_left <= ((((((mo - x1) / 18) - 1) * ((lo - y1) / 18)) << SHIFT_AMOUNT) * leftRatio) >>> SHIFT_AMOUNT;
			end
			
			if (((((((mo - x1) / 18) - 1) * ((lo - y1) / 18)) << SHIFT_AMOUNT) * rightRatio) >>> SHIFT_AMOUNT < 0) begin
				w_x1y2_right <= (((((((mo - x1) / 18) - 1) * ((lo - y1) / 18)) << SHIFT_AMOUNT) * rightRatio) >>> SHIFT_AMOUNT) * -1;
			end else begin
				w_x1y2_right <= ((((((mo - x1) / 18) - 1) * ((lo - y1) / 18)) << SHIFT_AMOUNT) * rightRatio) >>> SHIFT_AMOUNT;
			end
			
			if ((((((mo - x1) / 18) * (((lo - y1) / 18) - 1)) << SHIFT_AMOUNT) * leftRatio) >>> SHIFT_AMOUNT < 0) begin
				w_x2y1_left <= ((((((mo - x1) / 18) * (((lo - y1) / 18) - 1)) << SHIFT_AMOUNT) * leftRatio) >>> SHIFT_AMOUNT) * -1;
			end else begin
				w_x2y1_left <= (((((mo - x1) / 18) * (((lo - y1) / 18) - 1)) << SHIFT_AMOUNT) * leftRatio) >>> SHIFT_AMOUNT;
			end
			
			if ((((((mo - x1) / 18) * (((lo - y1) / 18) - 1)) << SHIFT_AMOUNT) * rightRatio) >>> SHIFT_AMOUNT < 0) begin
				w_x2y1_right <= ((((((mo - x1) / 18) * (((lo - y1) / 18) - 1)) << SHIFT_AMOUNT) * rightRatio) >>> SHIFT_AMOUNT) * -1;
			end else begin
				w_x2y1_right <= (((((mo - x1) / 18) * (((lo - y1) / 18) - 1)) << SHIFT_AMOUNT) * rightRatio) >>> SHIFT_AMOUNT;
			end
			
			if ((((((mo - x1) / 18) * ((lo - y1) / 18)) << SHIFT_AMOUNT) * leftRatio) >>> SHIFT_AMOUNT < 0) begin
				w_x2y2_left <= ((((((mo - x1) / 18) * ((lo - y1) / 18)) << SHIFT_AMOUNT) * leftRatio) >>> SHIFT_AMOUNT) * -1;
			end else begin
				w_x2y2_left <= (((((mo - x1) / 18) * ((lo - y1) / 18)) << SHIFT_AMOUNT) * leftRatio) >>> SHIFT_AMOUNT;
			end
			
			if ((((((mo - x1) / 18) * ((lo - y1) / 18)) << SHIFT_AMOUNT) * rightRatio) >>> SHIFT_AMOUNT < 0) begin
				w_x2y2_right <= ((((((mo - x1) / 18) * ((lo - y1) / 18)) << SHIFT_AMOUNT) * rightRatio) >>> SHIFT_AMOUNT) * -1;
			end else begin
				w_x2y2_right <= (((((mo - x1) / 18) * ((lo - y1) / 18)) << SHIFT_AMOUNT) * rightRatio) >>> SHIFT_AMOUNT;
			end
			
			state <= state + 1;
		end
		
		11: begin
			$fwrite(data_file,"w_x1y1_left:%d w_x1y1_right:%d\n",w_x1y1_left,w_x1y1_right);
			$fwrite(data_file,"w_x1y2_left:%d w_x1y2_right:%d\n",w_x1y2_left,w_x1y2_right);
			$fwrite(data_file,"w_x2y1_left:%d w_x2y1_right:%d\n",w_x2y1_left,w_x2y1_right);
			$fwrite(data_file,"w_x2y2_left:%d w_x2y2_right:%d\n",w_x2y2_left,w_x2y2_right);
			$fwrite(data_file,"weight:%d\n",weight);
			$fwrite(data_file,"histogram[y1][x1][leftBinIndex]:%d histogram[y1][x1][rightBinIndex]:%d\n",histogram[y1][x1][leftBinIndex],histogram[y1][x1][rightBinIndex]);
			$fwrite(data_file,"histogram[y2][x1][leftBinIndex]:%d histogram[y2][x1][rightBinIndex]:%d\n",histogram[y2][x1][leftBinIndex],histogram[y2][x1][rightBinIndex]);
			$fwrite(data_file,"histogram[y1][x2][leftBinIndex]:%d histogram[y1][x2][rightBinIndex]:%d\n",histogram[y1][x2][leftBinIndex],histogram[y1][x2][rightBinIndex]);
			$fwrite(data_file,"histogram[y2][x2][leftBinIndex]:%d histogram[y2][x2][rightBinIndex]:%d\n",histogram[y2][x2][leftBinIndex],histogram[y2][x2][rightBinIndex]);
								
								$fwrite(data_file,"w_x1y1_left * weight:%d\n",w_x1y1_left * weight);
								$fwrite(data_file,"w_x1y1_right * weight:%d\n",w_x1y1_right * weight);
								$fwrite(data_file,"w_x1y2_left * weight:%d\n",w_x1y2_left * weight);
								$fwrite(data_file,"w_x1y2_right * weight:%d\n",w_x1y2_right * weight);
								$fwrite(data_file,"w_x2y1_left * weight:%d\n",w_x2y1_left * weight);
								$fwrite(data_file,"w_x2y1_right * weight:%d\n",w_x2y1_right * weight);
								$fwrite(data_file,"w_x2y2_left * weight:%d\n",w_x2y2_left * weight);
								$fwrite(data_file,"w_x2y2_right * weight:%d\n",w_x2y2_right * weight);
								
								$fwrite(data_file,"(w_x1y1_left * weight)>>>SHIFT_AMOUNT:%d\n",(w_x1y1_left * weight)>>>SHIFT_AMOUNT);
								$fwrite(data_file,"(w_x1y1_right * weight)>>>SHIFT_AMOUNT:%d\n",(w_x1y1_right * weight)>>>SHIFT_AMOUNT);
								$fwrite(data_file,"(w_x1y2_left * weight)>>>SHIFT_AMOUNT:%d\n",(w_x1y2_left * weight)>>>SHIFT_AMOUNT);
								$fwrite(data_file,"(w_x1y2_right * weight)>>>SHIFT_AMOUNT:%d\n",(w_x1y2_right * weight)>>>SHIFT_AMOUNT);
								$fwrite(data_file,"(w_x2y1_left * weight)>>>SHIFT_AMOUNT:%d\n",(w_x2y1_left * weight)>>>SHIFT_AMOUNT);
								$fwrite(data_file,"(w_x2y1_right * weight)>>>SHIFT_AMOUNT:%d\n",(w_x2y1_right * weight)>>>SHIFT_AMOUNT);
								$fwrite(data_file,"(w_x2y2_left * weight)>>>SHIFT_AMOUNT:%d\n",(w_x2y2_left * weight)>>>SHIFT_AMOUNT);
								$fwrite(data_file,"(w_x2y2_right * weight)>>>SHIFT_AMOUNT:%d\n",(w_x2y2_right * weight)>>>SHIFT_AMOUNT);
			histogram[y1][x1][leftBinIndex] <= histogram[y1][x1][leftBinIndex] + 64'((128'(w_x1y1_left * weight) + ((128'(w_x1y1_left * weight) & 1 << (SHIFT_AMOUNT - 1)) << 1)) >>> SHIFT_AMOUNT);
			histogram[y1][x1][rightBinIndex] <= histogram[y1][x1][rightBinIndex] + 64'((128'(w_x1y1_right * weight) + ((128'(w_x1y1_right * weight) & 1 << (SHIFT_AMOUNT - 1)) << 1)) >>> SHIFT_AMOUNT);
			
			mo <= mo + 1;
			
			state <= state + 1;
		end
		
		12: begin
			histogram[y2][x1][leftBinIndex] <= histogram[y2][x1][leftBinIndex] + 64'((128'(w_x1y2_left * weight) + ((128'(w_x1y2_left * weight) & 1 << (SHIFT_AMOUNT - 1)) << 1)) >>> SHIFT_AMOUNT);
			histogram[y2][x1][rightBinIndex] <= histogram[y2][x1][rightBinIndex] + 64'((128'(w_x1y2_right * weight) + ((128'(w_x1y2_right * weight) & 1 << (SHIFT_AMOUNT - 1)) << 1)) >>> SHIFT_AMOUNT);
			if (mo >= jo + 18 || mo >= 128) begin
				lo <= lo + 1;
				mo <= jo;
				//$fwrite(data_file,"lo++ mo=jo\n");
			end
			
			state <= state + 1;
		end
		
		13: begin
			histogram[y1][x2][leftBinIndex] <= histogram[y1][x2][leftBinIndex] + 64'((128'(w_x2y1_left * weight) + ((128'(w_x2y1_left * weight) & 1 << (SHIFT_AMOUNT - 1)) << 1)) >>> SHIFT_AMOUNT);
			histogram[y1][x2][rightBinIndex] <= histogram[y1][x2][rightBinIndex] + 64'((128'(w_x2y1_right * weight) + ((128'(w_x2y1_right * weight) & 1 << (SHIFT_AMOUNT - 1)) << 1)) >>> SHIFT_AMOUNT);
			if (lo >= io + 18 || lo >= 128) begin
				jo <= jo + 8'd18;
				lo <= io;
				mo <= jo + 8'd18;
			//$fwrite(data_file,"jo+18 lo=io mo=jo\n");
			end
			
			state <= state + 1;
		end
		
		14: begin
			histogram[y2][x2][leftBinIndex] <= histogram[y2][x2][leftBinIndex] + 64'((128'(w_x2y2_left * weight) + ((128'(w_x2y2_left * weight) & 1 << (SHIFT_AMOUNT - 1)) << 1)) >>> SHIFT_AMOUNT);
			histogram[y2][x2][rightBinIndex] <= histogram[y2][x2][rightBinIndex] + 64'((128'(w_x2y2_right * weight) + ((128'(w_x2y2_right * weight) & 1 << (SHIFT_AMOUNT - 1)) << 1)) >>> SHIFT_AMOUNT);
			$fwrite(data_file,"histogram[y1][x1][leftBinIndex]:%d histogram[y1][x1][rightBinIndex]:%d\n",histogram[y1][x1][leftBinIndex],histogram[y1][x1][rightBinIndex]);
			$fwrite(data_file,"histogram[y2][x1][leftBinIndex]:%d histogram[y2][x1][rightBinIndex]:%d\n",histogram[y2][x1][leftBinIndex],histogram[y2][x1][rightBinIndex]);
			$fwrite(data_file,"histogram[y1][x2][leftBinIndex]:%d histogram[y1][x2][rightBinIndex]:%d\n",histogram[y1][x2][leftBinIndex],histogram[y1][x2][rightBinIndex]);
			$fwrite(data_file,"histogram[y2][x2][leftBinIndex]:%d histogram[y2][x2][rightBinIndex]:%d\n",histogram[y2][x2][leftBinIndex] + 64'((128'(w_x2y2_left * weight) + ((128'(w_x2y2_left * weight) & 1 << (SHIFT_AMOUNT - 1)) << 1)) >>> SHIFT_AMOUNT),histogram[y2][x2][rightBinIndex] + 64'((128'(w_x2y2_right * weight) + ((128'(w_x2y2_right * weight) & 1 << (SHIFT_AMOUNT - 1)) << 1)) >>> SHIFT_AMOUNT));
			if (jo > 128 - 18) begin
				io <= io + 8'd18;
				jo <= 0;
				lo <= io + 8'd18;
				mo <= 0;
			//$fwrite(data_file,"io+18 jo=0 lo+18 mo=0\n");
			end
			
			state <= 0;
		end
		
		15: begin
			if (io < 6) begin
				if (jo < 6) begin
					if (lo < io + 2 && io < 7) begin
						if (mo < jo + 2 && jo < 7) begin
							if (ko < 9) begin
			$fwrite(data_file,"histogram[%d][%d][%d]:%d\n",lo,mo,ko,histogram[lo][mo][ko]);
								//acumulator <= acumulator + ((histogram[lo][mo][ko] * histogram[lo][mo][ko]) >>> SHIFT_AMOUNT);
								acumulator <= acumulator + 64'((128'(histogram[lo][mo][ko] * histogram[lo][mo][ko]) + ((128'(histogram[lo][mo][ko] * histogram[lo][mo][ko]) & 1 << (SHIFT_AMOUNT - 1)) << 1)) >>> SHIFT_AMOUNT);
								//$fwrite(data_file, "i=%d j=%d l=%d m=%d k=%d acumulator=%h\n", io, jo, lo, mo, ko, acumulator);
						$fwrite(data_file, "histogram[lo][mo][ko] * histogram[lo][mo][ko]:%d\n", 128'(histogram[lo][mo][ko] * histogram[lo][mo][ko]));
						// $fwrite(data_file, "histogram[lo][mo][ko] * histogram[lo][mo][ko]:%d\n", 128'(histogram[lo][mo][ko]) * 128'(histogram[lo][mo][ko]));
						// $fwrite(data_file, "histogram[lo][mo][ko] * histogram[lo][mo][ko]:%d\n", 128'(128'(histogram[lo][mo][ko]) * 128'(histogram[lo][mo][ko])));
						$fwrite(data_file, "temp2 + ((temp2 & 1 << (SHIFT_AMOUNT - 1)) << 1):%d\n", 128'(histogram[lo][mo][ko] * histogram[lo][mo][ko]) + ((128'(histogram[lo][mo][ko] * histogram[lo][mo][ko]) & 1 << (SHIFT_AMOUNT - 1)) << 1));
						$fwrite(data_file, "temp2 >>>= SHIFT_AMOUNT:%d\n", (128'(histogram[lo][mo][ko] * histogram[lo][mo][ko]) + ((128'(histogram[lo][mo][ko] * histogram[lo][mo][ko]) & 1 << (SHIFT_AMOUNT - 1)) << 1)) >>> SHIFT_AMOUNT);
						$fwrite(data_file, "acumulator:%d\n", acumulator + 64'((128'(histogram[lo][mo][ko] * histogram[lo][mo][ko]) + ((128'(histogram[lo][mo][ko] * histogram[lo][mo][ko]) & 1 << (SHIFT_AMOUNT - 1)) << 1)) >>> SHIFT_AMOUNT));
								
								ko <= ko + 1;
							end else begin
								mo <= mo + 1;
								ko <= 0;
							end
						end else begin
							lo <= lo + 1;
							mo <= jo;
							ko <= 0;
						end
					end else begin
						//jo++;
						lo <= io;
						mo <= jo;
						ko <= 0;			
						//acumulator = 128'h1400000;
						in <= acumulator;		
						state <= state + 1;
						$fwrite(data_file, "acumulator:%d\n", acumulator);
					end
				end else begin
					io <= io + 1;
					jo <= 0;
					lo <= io + 1;
					mo <= mo;
					ko <= 0;
				end
			end else begin
				$fwrite(data_file, "end\n");
				
				$fclose(data_file);
				$stop;
			end
		end
		
		16: begin
			if (out < 0) begin
				aux2 <= acumulator << $unsigned(out * -1);
			end else begin
				aux2 <= acumulator >> $unsigned(out);
			end
			e <= 8'(out + 127);
			
			state <= state + 1;
		end
		
		17: begin
			e <= 8'(((128'h5F3759DF - (({1'b0,8'(e),23'(aux2 >>> (SHIFT_AMOUNT - 23))}) >> 1)) >>> 23) - 127);
			aux2 <= (1 << SHIFT_AMOUNT) + (23'(128'h5F3759DF - (({1'b0,8'(e),23'(aux2 >>> (SHIFT_AMOUNT - 23))}) >> 1)) << (SHIFT_AMOUNT - 23));
			
			state <= state + 1;
		end
		
		18: begin
			if (e < 0) begin
				temp2 <= aux2 >> $unsigned(e * -1);
			end else begin
				temp2 <= aux2 << $unsigned(e);
			end
			state <= state + 1;
		end
		
		19: begin
			// temp2 = a * b
			//temp2 <= ((a * b) + (((a * b) & 1 << (SHIFT_AMOUNT - 1)) << 1)) >>> SHIFT_AMOUNT;
			//y = y * ( threehalfs - ( x2 * y * y ) )
			//y * y
			// aux2 <= ((temp2 * temp2) + (((temp2 * temp2) & 1 << (SHIFT_AMOUNT - 1)) << 1)) >>> SHIFT_AMOUNT;
			//number * y * y
			// temp2 <= (((((temp2 * temp2) + (((temp2 * temp2) & 1 << (SHIFT_AMOUNT - 1)) << 1)) >>> SHIFT_AMOUNT) * acumulator) + ((((((temp2 * temp2) + (((temp2 * temp2) & 1 << (SHIFT_AMOUNT - 1)) << 1)) >>> SHIFT_AMOUNT) * acumulator) & 1 << (SHIFT_AMOUNT - 1)) << 1)) >>> SHIFT_AMOUNT;
			//threehalfs - ( x2 * y * y )
			// temp2 <= (128'hc000000 - (((((((((temp2 * temp2) + (((temp2 * temp2) & 1 << (SHIFT_AMOUNT - 1)) << 1)) >>> SHIFT_AMOUNT) * acumulator) + ((((((temp2 * temp2) + (((temp2 * temp2) & 1 << (SHIFT_AMOUNT - 1)) << 1)) >>> SHIFT_AMOUNT) * acumulator) & 1 << (SHIFT_AMOUNT - 1)) << 1)) >>> SHIFT_AMOUNT) * 128'h4000000) + (((((((((temp2 * temp2) + (((temp2 * temp2) & 1 << (SHIFT_AMOUNT - 1)) << 1)) >>> SHIFT_AMOUNT) * acumulator) + ((((((temp2 * temp2) + (((temp2 * temp2) & 1 << (SHIFT_AMOUNT - 1)) << 1)) >>> SHIFT_AMOUNT) * acumulator) & 1 << (SHIFT_AMOUNT - 1)) << 1)) >>> SHIFT_AMOUNT) * 128'h4000000) & 1 << (SHIFT_AMOUNT - 1)) << 1)) >>> SHIFT_AMOUNT));
			//y  = y * ( threehalfs - ( x2 * y * y ) )
			// temp2 <= ((temp2 * (128'hc000000 - (((((((((temp2 * temp2) + (((temp2 * temp2) & 1 << (SHIFT_AMOUNT - 1)) << 1)) >>> SHIFT_AMOUNT) * acumulator) + ((((((temp2 * temp2) + (((temp2 * temp2) & 1 << (SHIFT_AMOUNT - 1)) << 1)) >>> SHIFT_AMOUNT) * acumulator) & 1 << (SHIFT_AMOUNT - 1)) << 1)) >>> SHIFT_AMOUNT) * 128'h4000000) + (((((((((temp2 * temp2) + (((temp2 * temp2) & 1 << (SHIFT_AMOUNT - 1)) << 1)) >>> SHIFT_AMOUNT) * acumulator) + ((((((temp2 * temp2) + (((temp2 * temp2) & 1 << (SHIFT_AMOUNT - 1)) << 1)) >>> SHIFT_AMOUNT) * acumulator) & 1 << (SHIFT_AMOUNT - 1)) << 1)) >>> SHIFT_AMOUNT) * 128'h4000000) & 1 << (SHIFT_AMOUNT - 1)) << 1)) >>> SHIFT_AMOUNT))) + (((temp2 * (128'hc000000 - (((((((((temp2 * temp2) + (((temp2 * temp2) & 1 << (SHIFT_AMOUNT - 1)) << 1)) >>> SHIFT_AMOUNT) * acumulator) + ((((((temp2 * temp2) + (((temp2 * temp2) & 1 << (SHIFT_AMOUNT - 1)) << 1)) >>> SHIFT_AMOUNT) * acumulator) & 1 << (SHIFT_AMOUNT - 1)) << 1)) >>> SHIFT_AMOUNT) * 128'h4000000) + (((((((((temp2 * temp2) + (((temp2 * temp2) & 1 << (SHIFT_AMOUNT - 1)) << 1)) >>> SHIFT_AMOUNT) * acumulator) + ((((((temp2 * temp2) + (((temp2 * temp2) & 1 << (SHIFT_AMOUNT - 1)) << 1)) >>> SHIFT_AMOUNT) * acumulator) & 1 << (SHIFT_AMOUNT - 1)) << 1)) >>> SHIFT_AMOUNT) * 128'h4000000) & 1 << (SHIFT_AMOUNT - 1)) << 1)) >>> SHIFT_AMOUNT))) & 1 << (SHIFT_AMOUNT - 1)) << 1)) >>> SHIFT_AMOUNT;
			//2635760036
			// sqrt_acumulator <= (((((temp2 * (128'hc000000 - (((((((((temp2 * temp2) + (((temp2 * temp2) & 1 << (SHIFT_AMOUNT - 1)) << 1)) >>> SHIFT_AMOUNT) * acumulator) + ((((((temp2 * temp2) + (((temp2 * temp2) & 1 << (SHIFT_AMOUNT - 1)) << 1)) >>> SHIFT_AMOUNT) * acumulator) & 1 << (SHIFT_AMOUNT - 1)) << 1)) >>> SHIFT_AMOUNT) * 128'h4000000) + (((((((((temp2 * temp2) + (((temp2 * temp2) & 1 << (SHIFT_AMOUNT - 1)) << 1)) >>> SHIFT_AMOUNT) * acumulator) + ((((((temp2 * temp2) + (((temp2 * temp2) & 1 << (SHIFT_AMOUNT - 1)) << 1)) >>> SHIFT_AMOUNT) * acumulator) & 1 << (SHIFT_AMOUNT - 1)) << 1)) >>> SHIFT_AMOUNT) * 128'h4000000) & 1 << (SHIFT_AMOUNT - 1)) << 1)) >>> SHIFT_AMOUNT))) + (((temp2 * (128'hc000000 - (((((((((temp2 * temp2) + (((temp2 * temp2) & 1 << (SHIFT_AMOUNT - 1)) << 1)) >>> SHIFT_AMOUNT) * acumulator) + ((((((temp2 * temp2) + (((temp2 * temp2) & 1 << (SHIFT_AMOUNT - 1)) << 1)) >>> SHIFT_AMOUNT) * acumulator) & 1 << (SHIFT_AMOUNT - 1)) << 1)) >>> SHIFT_AMOUNT) * 128'h4000000) + (((((((((temp2 * temp2) + (((temp2 * temp2) & 1 << (SHIFT_AMOUNT - 1)) << 1)) >>> SHIFT_AMOUNT) * acumulator) + ((((((temp2 * temp2) + (((temp2 * temp2) & 1 << (SHIFT_AMOUNT - 1)) << 1)) >>> SHIFT_AMOUNT) * acumulator) & 1 << (SHIFT_AMOUNT - 1)) << 1)) >>> SHIFT_AMOUNT) * 128'h4000000) & 1 << (SHIFT_AMOUNT - 1)) << 1)) >>> SHIFT_AMOUNT))) & 1 << (SHIFT_AMOUNT - 1)) << 1)) >>> SHIFT_AMOUNT) * acumulator) + ((((((temp2 * (128'hc000000 - (((((((((temp2 * temp2) + (((temp2 * temp2) & 1 << (SHIFT_AMOUNT - 1)) << 1)) >>> SHIFT_AMOUNT) * acumulator) + ((((((temp2 * temp2) + (((temp2 * temp2) & 1 << (SHIFT_AMOUNT - 1)) << 1)) >>> SHIFT_AMOUNT) * acumulator) & 1 << (SHIFT_AMOUNT - 1)) << 1)) >>> SHIFT_AMOUNT) * 128'h4000000) + (((((((((temp2 * temp2) + (((temp2 * temp2) & 1 << (SHIFT_AMOUNT - 1)) << 1)) >>> SHIFT_AMOUNT) * acumulator) + ((((((temp2 * temp2) + (((temp2 * temp2) & 1 << (SHIFT_AMOUNT - 1)) << 1)) >>> SHIFT_AMOUNT) * acumulator) & 1 << (SHIFT_AMOUNT - 1)) << 1)) >>> SHIFT_AMOUNT) * 128'h4000000) & 1 << (SHIFT_AMOUNT - 1)) << 1)) >>> SHIFT_AMOUNT))) + (((temp2 * (128'hc000000 - (((((((((temp2 * temp2) + (((temp2 * temp2) & 1 << (SHIFT_AMOUNT - 1)) << 1)) >>> SHIFT_AMOUNT) * acumulator) + ((((((temp2 * temp2) + (((temp2 * temp2) & 1 << (SHIFT_AMOUNT - 1)) << 1)) >>> SHIFT_AMOUNT) * acumulator) & 1 << (SHIFT_AMOUNT - 1)) << 1)) >>> SHIFT_AMOUNT) * 128'h4000000) + (((((((((temp2 * temp2) + (((temp2 * temp2) & 1 << (SHIFT_AMOUNT - 1)) << 1)) >>> SHIFT_AMOUNT) * acumulator) + ((((((temp2 * temp2) + (((temp2 * temp2) & 1 << (SHIFT_AMOUNT - 1)) << 1)) >>> SHIFT_AMOUNT) * acumulator) & 1 << (SHIFT_AMOUNT - 1)) << 1)) >>> SHIFT_AMOUNT) * 128'h4000000) & 1 << (SHIFT_AMOUNT - 1)) << 1)) >>> SHIFT_AMOUNT))) & 1 << (SHIFT_AMOUNT - 1)) << 1)) >>> SHIFT_AMOUNT) * acumulator) & 1 << (SHIFT_AMOUNT - 1)) << 1)) >>> SHIFT_AMOUNT;
			//2639523113, desejado = 2639519632
			sqrt_acumulator <= (((((((temp2 * (128'hc000000 - ((((((temp2 * temp2) >>> SHIFT_AMOUNT) * acumulator) >>> SHIFT_AMOUNT) * 128'h4000000) >>> SHIFT_AMOUNT))) >>> SHIFT_AMOUNT) * (128'hc000000 - ((((((((temp2 * (128'hc000000 - ((((((temp2 * temp2) >>> SHIFT_AMOUNT) * acumulator) >>> SHIFT_AMOUNT) * 128'h4000000) >>> SHIFT_AMOUNT))) >>> SHIFT_AMOUNT) * ((temp2 * (128'hc000000 - ((((((temp2 * temp2) >>> SHIFT_AMOUNT) * acumulator) >>> SHIFT_AMOUNT) * 128'h4000000) >>> SHIFT_AMOUNT))) >>> SHIFT_AMOUNT)) >>> SHIFT_AMOUNT) * acumulator) >>> SHIFT_AMOUNT) * 128'h4000000) >>> SHIFT_AMOUNT))) >>> SHIFT_AMOUNT) * (128'hc000000 - ((((((((((temp2 * (128'hc000000 - ((((((temp2 * temp2) >>> SHIFT_AMOUNT) * acumulator) >>> SHIFT_AMOUNT) * 128'h4000000) >>> SHIFT_AMOUNT))) >>> SHIFT_AMOUNT) * (128'hc000000 - ((((((((temp2 * (128'hc000000 - ((((((temp2 * temp2) >>> SHIFT_AMOUNT) * acumulator) >>> SHIFT_AMOUNT) * 128'h4000000) >>> SHIFT_AMOUNT))) >>> SHIFT_AMOUNT) * ((temp2 * (128'hc000000 - ((((((temp2 * temp2) >>> SHIFT_AMOUNT) * acumulator) >>> SHIFT_AMOUNT) * 128'h4000000) >>> SHIFT_AMOUNT))) >>> SHIFT_AMOUNT)) >>> SHIFT_AMOUNT) * acumulator) >>> SHIFT_AMOUNT) * 128'h4000000) >>> SHIFT_AMOUNT))) >>> SHIFT_AMOUNT) * ((((temp2 * (128'hc000000 - ((((((temp2 * temp2) >>> SHIFT_AMOUNT) * acumulator) >>> SHIFT_AMOUNT) * 128'h4000000) >>> SHIFT_AMOUNT))) >>> SHIFT_AMOUNT) * (128'hc000000 - ((((((((temp2 * (128'hc000000 - ((((((temp2 * temp2) >>> SHIFT_AMOUNT) * acumulator) >>> SHIFT_AMOUNT) * 128'h4000000) >>> SHIFT_AMOUNT))) >>> SHIFT_AMOUNT) * ((temp2 * (128'hc000000 - ((((((temp2 * temp2) >>> SHIFT_AMOUNT) * acumulator) >>> SHIFT_AMOUNT) * 128'h4000000) >>> SHIFT_AMOUNT))) >>> SHIFT_AMOUNT)) >>> SHIFT_AMOUNT) * acumulator) >>> SHIFT_AMOUNT) * 128'h4000000) >>> SHIFT_AMOUNT))) >>> SHIFT_AMOUNT)) >>> SHIFT_AMOUNT) * acumulator) >>> SHIFT_AMOUNT) * 128'h4000000) >>> SHIFT_AMOUNT))) >>> SHIFT_AMOUNT) * acumulator) >>> SHIFT_AMOUNT;
			
			//sqrt_acumulator <= (temp2 * acumulator) >>> SHIFT_AMOUNT;
			
			state <= state + 1;
		end
		
		20: begin
		//$fwrite(data_file, "sqrt_acumulator:%d\n", sqrt_acumulator);
			if (lo < io + 2 && io < 7) begin
				if (mo < jo + 2 && jo < 7) begin
					if (ko < 9) begin
						if (((histogram[lo][mo][ko] << SHIFT_AMOUNT) / sqrt_acumulator) > 128'h1999999) begin
							featuresVector_out <= 128'h1999999;
						end else begin
							featuresVector_out <= (histogram[lo][mo][ko] << SHIFT_AMOUNT) / sqrt_acumulator;
						end
						
						if (end_signal == 0) begin
							end_signal <= 1;
							state <= state + 1;
						end
			//$fwrite(data_file,"=ko:%d ",ko);
						
						ko <= ko + 1;
					end else begin
						mo <= mo + 1;
						ko <= 0;
					end
				end else begin
					lo <= lo + 1;
					mo <= jo;
					ko <= 0;
				end
			end else begin
				jo <= jo + 1;
				lo <= io;
				mo <= jo + 1;
				ko <= 0;
				acumulator <= 0;
				state <= 15;
			end
		end
		
		21: begin
			if (end_signal == 1) begin
				//$fwrite(data_file,"io:%d jo:%d lo:%d mo:%d ko:%d ",io,jo,lo,mo,ko);
				$fwrite(data_file, "000000000000000%h\n", featuresVector_out);
				
				end_signal <= 0;
			end
			state <= state - 1;
		end
		endcase
	end
endmodule

module highbit #(
	parameter OUT_WIDTH = 8, // out uses one extra bit for not-found
	parameter IN_WIDTH = 1<<(OUT_WIDTH-1)
) (
	input [IN_WIDTH-1:0]in,
	output [OUT_WIDTH-1:0]out
);

wire [OUT_WIDTH-1:0]out_stage[0:IN_WIDTH];
assign out_stage[0] = ~8'b0; // desired default output if no bits set
genvar i;
generate
	for(i=0; i<IN_WIDTH; i=i+1) begin : high_bit_finder
		assign out_stage[i+1] = in[i] ? 8'(i - 27) : out_stage[i]; 
	end
endgenerate
assign out = out_stage[IN_WIDTH];

endmodule