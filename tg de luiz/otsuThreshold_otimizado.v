module otsuThreshold(
		input clk, 
		input rst,
		input start, 
		input logic [31:0] in, 
		input logic [127:0] threshold, 
		input toOtsuThreshold, 
		input logic [31:0] q_a, 
		output logic [11:0] address_a, 
		output logic [11:0] address_b, 
		output logic [31:0] data_b, 
		output logic wren_b, 
		output logic toHistogramBins,
		output logic [31:0] binary_out,
		output logic binary_valid
	);
	parameter SHIFT_AMOUNT = 24;
	parameter SHIFT_MASK = ((1 << SHIFT_AMOUNT) - 1);
	
	logic [127:0] pixel1, pixel2, pixel3, pixel4;
	//bit [16383:0][15:0] image;
	logic [15:0] count, i, j;
	logic [7:0] i1, i2, i3, i4;
	logic [7:0] image_a, image_b;
	//logic [31:0] temp;
	//logic out;
	
	integer data_file;
	integer print_file;
	
	initial begin
		count = 0;
		i = 0;
		j = 0;
		address_a = 0;
		address_b = 0;
		toHistogramBins = 0;
		
		data_file = $fopen("otsuThreshold.txt", "w");
		if (data_file == 0) begin
			$display("data_file handle was NULL");
			$finish;
		end
	end

	always @(posedge clk) begin
		if (rst) begin
			count = 0;
			i = 0;
			j = 0;
			address_a = 0;
			address_b = 0;
			toHistogramBins = 0;
		end
		
		if (wren_b) begin
			wren_b = 0;
		end
		
		if (binary_valid) begin
			binary_valid = 0;
		end
		
		if (start) begin		
			if (count < 4096) begin
				address_b = 12'(i);
				data_b = in;
				wren_b = 1;
				
				count++;
				i++;
			end
		end
		
		if (toOtsuThreshold) begin
			if (j < 4096 + 3) begin
				if (j < 4096) begin
					address_a = 12'(j);
				end
				
				//i1 = 8'(q_a >> 24);
				//i2 = 8'((q_a >> 16) & 32'hFF);
				//i3 = 8'((q_a >> 8) & 32'hFF);
				//i4 = 8'(q_a & 32'hFF);
				
				binary_out = 0;
			
				if (j >= 3) begin
					pixel1 = ((q_a[31:24] << SHIFT_AMOUNT) << SHIFT_AMOUNT) / (255 << SHIFT_AMOUNT);
					if (pixel1 >= threshold) begin
						binary_out[31:24] = 1;
					end else begin
						binary_out[31:24] = 0;
					end
					$fwrite(data_file, "j=%d pixel= %h out=%b\n", (j-3)*4, pixel1, binary_out[31:24]);
					pixel2 = ((q_a[23:16] << SHIFT_AMOUNT) << SHIFT_AMOUNT) / (255 << SHIFT_AMOUNT);
					if (pixel2 >= threshold) begin
						binary_out[23:16] = 1;
					end else begin
						binary_out[23:16] = 0;
					end
					$fwrite(data_file, "j=%d pixel= %h out=%b\n", (j-3)*4+1, pixel2, binary_out[23:16]);
					pixel3 = ((q_a[15:8] << SHIFT_AMOUNT) << SHIFT_AMOUNT) / (255 << SHIFT_AMOUNT);
					if (pixel3 >= threshold) begin
						binary_out[15:8] = 1;
					end else begin
						binary_out[15:8] = 0;
					end
					$fwrite(data_file, "j=%d pixel= %h out=%b\n", (j-3)*4+2, pixel3, binary_out[15:8]);
					pixel4 = ((q_a[7:0] << SHIFT_AMOUNT) << SHIFT_AMOUNT) / (255 << SHIFT_AMOUNT);
					if (pixel4 >= threshold) begin
						binary_out[7:0] = 1;
					end else begin
						binary_out[7:0] = 0;
					end
					$fwrite(data_file, "j=%d pixel= %h out=%b\n", (j-3)*4+3, pixel4, binary_out[7:0]);
					//$fwrite(data_file, "%h\n", binary_out);
					
					//address_b = 12'(j - 3);
					//binary_out = binary_out;
					if (binary_valid == 0) begin
						binary_valid = 1;
					end
					//wren_b = 1;
				end
				
				//$fwrite(data_file, "j=%d pixel=%h out=%b\n", j, pixel, out);
				
				j++;
			end else begin
				toHistogramBins = 1;
				address_a = 0;
				address_b = 0;
				$stop;
			end
		end
	end
endmodule
