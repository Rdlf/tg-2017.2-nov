module otsuThreshold(
		input clk, 
		input rst,
		input start, 
		input [31:0] in, 
		input [127:0] threshold, 
		input toOtsuThreshold, 
		input int q_a, 
		output bit [11:0] address_a, 
		output bit [11:0] address_b, 
		output int data_b, 
		output bit wren_b, 
		output bit toHistogramBins
	);
	parameter SHIFT_AMOUNT = 24;
	parameter SHIFT_MASK = ((1 << SHIFT_AMOUNT) - 1);
	
	bit [127:0] pixel;
	//bit [16383:0][15:0] image;
	shortint count, i, j;
	byte unsigned i1, i2, i3, i4;
	byte image_a, image_b;
	int temp;
	bit out;
	
// 	integer data_file;
//	integer print_file;
	
	initial begin
		count = 0;
		i = 0;
		j = 0;
		address_a = 0;
		address_b = 0;
		toHistogramBins = 0;
		
// 		data_file = $fopen("otsuThreshold.txt", "w");
//		if (data_file == 0) begin
//			$display("data_file handle was NULL");
//			$finish;
//		end
	end

	always @(posedge clk) begin
		if (rst) begin
			count = 0;
			i = 0;
			j = 0;
			address_a = 0;
			address_b = 0;
			toHistogramBins = 0;
		end
		
		if (wren_b) begin
			wren_b = 0;
		end
		
		if (start) begin		
			if (count < 4096) begin
				address_b = 12'(i);
				data_b = in;
				wren_b = 1;
				
				count++;
				i++;
			end
		end
		
		if (toOtsuThreshold) begin
			if (j < 4096 + 3) begin
				if (j < 4096) begin
					address_a = 12'(j);
				end
				
				i1 = 8'(q_a >> 24);
				i2 = 8'((q_a >> 16) & 32'hFF);
				i3 = 8'((q_a >> 8) & 32'hFF);
				i4 = 8'(q_a & 32'hFF);
				
				temp = 0;
			
				if (j >= 3) begin
					pixel = ((i1 << SHIFT_AMOUNT) << SHIFT_AMOUNT) / (255 << SHIFT_AMOUNT);
					if (pixel >= threshold) begin
						out = 1;
					end else begin
						out = 0;
					end
					temp = temp | out << 24;
//					$fwrite(data_file, "j=%d pixel= %h out=%b\n", (j-3)*4, pixel, out);
					pixel = ((i2 << SHIFT_AMOUNT) << SHIFT_AMOUNT) / (255 << SHIFT_AMOUNT);
					if (pixel >= threshold) begin
						out = 1;
					end else begin
						out = 0;
					end
					temp = temp | out << 16;
//					$fwrite(data_file, "j=%d pixel= %h out=%b\n", (j-3)*4+1, pixel, out);
					pixel = ((i3 << SHIFT_AMOUNT) << SHIFT_AMOUNT) / (255 << SHIFT_AMOUNT);
					if (pixel >= threshold) begin
						out = 1;
					end else begin
						out = 0;
					end
					temp = temp | out << 8;
//					$fwrite(data_file, "j=%d pixel= %h out=%b\n", (j-3)*4+2, pixel, out);
					pixel = ((i4 << SHIFT_AMOUNT) << SHIFT_AMOUNT) / (255 << SHIFT_AMOUNT);
					if (pixel >= threshold) begin
						out = 1;
					end else begin
						out = 0;
					end
					temp = temp | out;
//					$fwrite(data_file, "j=%d pixel= %h out=%b\n", (j-3)*4+3, pixel, out);
					//$fwrite(data_file, "temp=%h", temp);
					
					address_b = 12'(j - 3);
					data_b = temp;
					wren_b = 1;
				end
				
				//$fwrite(data_file, "j=%d pixel=%h out=%b\n", j, pixel, out);
				
				j++;
			end else begin
				toHistogramBins = 1;
				address_a = 0;
				address_b = 0;
			end
		end
	end
endmodule
