module histogramGrayScale #(parameter GRAYSCALE_LEVELS = 255)(input [31:0] in, input clk, input rst, output reg [31:0] out, count);
	bit [GRAYSCALE_LEVELS-1:0][31:0] histogram;
	byte i1, i2, i3, i4;
	byte o1, o2;
	
	always @(in or posedge clk or rst) begin
		if (rst) begin
			for (int i = 0; i < GRAYSCALE_LEVELS; i++) begin
				histogram[i] = 0;
			end
			count = 0;
			o1 = 0;
			o2 = 0;
		end
		
		i1 = (in >> 24);
		i2 = ((in >> 16) & 32'hFF);
		i3 = ((in >> 8) & 32'hFF);
		i4 = (in & 32'hFF);
		
		histogram[i1]++;
		histogram[i2]++;
		histogram[i3]++;
		histogram[i4]++;
		
		count++;
		out = count;
		
		//4096 = (128x128)x8bits/32bits
//		if (count == 4096) begin
//			for (int i = 0; i <= GRAYSCALE_LEVELS - 2; i = i + 2) begin
//				o1 = histogram[i];
//				o2 = histogram[i + 1];
//				
//				out = {o1, o2};
//			end
//		end
	end
endmodule
